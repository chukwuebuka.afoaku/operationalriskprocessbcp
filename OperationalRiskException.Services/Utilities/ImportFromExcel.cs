﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Utilities
{
	public  class ImportFromExcelToQuestionTable
	{
		private 
		ImportFromExcelToQuestionTable() {

		}

		public  int GetAllRecords( Stream FileUpload, string batchNo, int categoryId) {

			IDictionary<int, string> mapper = new Dictionary<int, string>();
			List<Question> records = new List<Question>();
			ISheet sheet;
			XSSFWorkbook hssfwb = new XSSFWorkbook(FileUpload);
			sheet = hssfwb.GetSheetAt(0);
			IRow headerRow = sheet.GetRow(0);
			int cellCount = headerRow.LastCellNum;
			//getting the header
			for (int j = 0; j < cellCount; j++)
			{
				ICell cell = headerRow.GetCell(j);
				if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
				mapper.Add(j, cell.ToString());
				//headers.Add(cell.ToString());
			}
			for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
			{
				var entity = new Question();
				IRow row = sheet.GetRow(i);
				if (row == null) continue;
				if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
				for (int j = row.FirstCellNum; j < cellCount; j++)
				{
					if (row.GetCell(j) != null)
					{
						entity.QuestionText = row.GetCell(j).ToString();
					}
				}
				entity.BatchNo = batchNo;
				entity.QuestionCategoryId = categoryId;
				
				
			
			}
			return 1;
		}
	}
}
