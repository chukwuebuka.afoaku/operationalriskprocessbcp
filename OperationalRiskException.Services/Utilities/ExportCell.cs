﻿using System;

namespace OperationalRiskException.Services.Utilities
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class ExportCell : Attribute
    {
        public string HeaderName { get; set; }
    }
}