﻿using System;

namespace OperationalRiskException.Services.Utilities
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class ExportCellIgnore : Attribute
    {
        public int Id { get; set; }

    }
}