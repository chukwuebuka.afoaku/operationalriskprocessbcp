﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.Context;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Data.UnitOfWork;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;

namespace OperationalRiskException.Services.Services
{
    public class TestResultService : ITestResultService
    {
        private IRepository<Question> _questionRepository;
        private IRepository<AnswerEntity> _testresultRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        private IRepository<BatchUpload> _batchRepo;
        private IRepository<Approval> _approvalRepo;
        private IRepository<User> _userRepository;
        private IUnitOfWork _unitOfWork;

        private IRepository<AssessmentScheduling> _assessmentScheduling;

        public TestResultService()
        {
            _questionRepository = new Repository<Question>();
            _testresultRepository = new Repository<AnswerEntity>();
            _approvalRepo = new Repository<Approval>();
            _batchRepo = new Repository<BatchUpload>();
            _userRepository = new Repository<User>();
            _assessmentScheduling = new Repository<AssessmentScheduling>();
            _unitOfWork = new UnitOfWork(new OperationalRiskContext());
        }
        public List<AssessmentReviewVM> PendingAssessments(DateTime startDate, DateTime endDate)
        {

            // var pendingApproval = _assessmentScheduling.GetAllIncluding(x => x.IsAnswered == true && x.ApprovalStatus==ApprovalStatus.Pending, false, x=>x.User);
            // 
            endDate = endDate.AddHours(23).AddMinutes(59);
            var pendingApproval = _assessmentScheduling.GetAllIncluding(x => x.IsAnswered == true && x.AssessmentDateCompleted<endDate && startDate<x.AssessmentDateCompleted && x.ApprovalStatus == ApprovalStatus.Pending, false, x => x.User);
            var assess = pendingApproval.Select(x => new AssessmentReviewVM
            {
                ApprovalStatus = x.ApprovalStatus,
                Username = x.User.Username,
                Id = x.Id,
                AssessmentSetupId = x.AssessmentSetupId,
                Department = x.User.DepartmentName,
                UserId = x.UserId,
                DateCompleted = x.AssessmentDateCompleted

                // DateCompleted = x.



            });
           // return assess.ToList().OrderBy(x => x.AssessmentDateCompleted).ToList();
            return assess.ToList();



            //using (var dbContext = new OperationalRiskContext())
            //{
            //    string query = $"Select * from AssessmentSchedulings AS ASCH " +
            //         "INNER JOIN Users  AS US US.Id = ASCH.UserId " +
            //         "INNER JOIN AnswerEntities AS AE on US.Id = AE.UserId " +
            //         "where  ASCH.ApprovalStatus = 0 AND ASCH.IsAnswered = 1 AND " +
            //         $"AE.CreatedDate between {startDate.ToString("yyyy-MM-dd")} AND {endDate.ToString("yyyy-MM-dd")}";

            //    var result   =dbContext.Database.SqlQuery<object[]>(query);


            //    var pendingApproval = (from q in dbContext.AssessmentSchedulings
            //                           join u in dbContext.Users on q.UserId equals u.Id
            //                           join k in dbContext.AnswerEntities on q.UserId equals k.UserId
            //                           where (k.CreatedDate <= startDate && k.CreatedDate <= endDate) && q.IsAnswered && q.ApprovalStatus == ApprovalStatus.Pending
            //                           group new { u, q } by new { q.Id, q.ApprovalStatus, q.AssessmentSetupId, u.Username, u.DepartmentName, UserId = u.Id, q.CreatedDate } into g

            //                           select new
            //                           {
            //                               g.Key.ApprovalStatus,
            //                               g.Key.Username,
            //                               g.Key.Id,
            //                               g.Key.DepartmentName,
            //                               g.Key.UserId,
            //                               g.Key.AssessmentSetupId,
            //                               DateCompleted = g.Key.CreatedDate,
            //                           }
            //                          ).ToList();
            //    //var pendingApproval= dbContext.


            //    var assess = pendingApproval.Select(x => new AssessmentReviewVM
            //    {
            //        ApprovalStatus = x.ApprovalStatus,
            //        Username = x.Username,
            //        Id = x.Id,
            //        AssessmentSetupId = x.AssessmentSetupId,
            //        Department = x.DepartmentName,
            //        UserId = x.UserId,
            //        DateCompleted = x.DateCompleted

            //        // DateCompleted = x.

            //    });
            //    //var assess = pendingApproval.Select(x => new AssessmentReviewVM
            //    //{
            //    //    ApprovalStatus = x.ApprovalStatus,
            //    //    Username = x.User.Username,
            //    //    Id = x.Id,
            //    //    AssessmentSetupId = x.AssessmentSetupId,
            //    //    Department = x.User.DepartmentName,
            //    //    UserId = x.UserId,
            //    //    DateCompleted = x.CreatedDate

            //    //    // DateCompleted = x.

            //    //});
            //    return assess.ToList();
            //}
        }

        public List<AssessmentReviewVM> RejectedAssessments()
        {
            var pendingApproval = _assessmentScheduling.GetAllIncluding(x => x.ApprovalStatus == ApprovalStatus.Rejected && x.IsAnswered == false, false, x => x.User);
            var assess = pendingApproval.Select(x => new AssessmentReviewVM
            {
                ApprovalStatus = x.ApprovalStatus,
                Username = x.User.Username,
                Id = x.Id,
                AssessmentSetupId = x.AssessmentSetupId,
                Department = x.User.DepartmentName,
                UserId = x.UserId

            });
            return assess.ToList();
        }

        public List<AssessmentReviewVM> ApprovedAssessments()
        {
            var pendingApproval = _assessmentScheduling.GetAllIncluding(x => x.ApprovalStatus == ApprovalStatus.Approved && x.IsAnswered, false, x => x.User);
            var assess = pendingApproval.Select(x => new AssessmentReviewVM
            {
                ApprovalStatus = x.ApprovalStatus,
                Username = x.User.Username,
                //SOLID = x.User.SOLId,
                Id = x.Id,
                AssessmentSetupId = x.AssessmentSetupId,
                Department = x.User.DepartmentName,
                UserId = x.UserId

            });
            return assess.ToList();
        }


        //chuks assignment to generate report here
        public List<AssessmentReport> ReportingGenerator()
        {
            var pendingApproval = _assessmentScheduling.GetAllIncluding(x =>(x.ApprovalStatus==ApprovalStatus.Approved || x.ApprovalStatus == ApprovalStatus.Rejected), false, x => x.User);
            var assess = pendingApproval.Select(x => new AssessmentReport
            {

                Username = x.User.Username,
                SOLID = x.User.SOLId,

                Department = x.User.DepartmentName,
                ApprovedDate = x.ApprovedDate,
                ApprovedBy = x.ApprovedBy,
                RejectedBy = x.RejectedBy,
                RejectedDate = x.RejectedDate,
                ApprovalStatus = x.ApprovalStatus.ToString(),
                DateCompleted = x.CreatedDate
                //    SOLID=x.User,


            }) ;
            var list = assess.ToList();
            return list;
        }

        public bool ApproveOrRejectAssessment( int schedulerId, ApprovalStatus status, string Username)
        {
            try
            {
                var checkScheduler = _assessmentScheduling.GetSingleRecordFilter(x => !x.IsDeleted &&  x.Id == schedulerId);
                if (checkScheduler == null)
                    throw new Exception("There is no  scheduled for the ");
                if (status == ApprovalStatus.Approved)
                {
                    checkScheduler.IsAnswered = true;
                    checkScheduler.ApprovedBy = Username;
                    checkScheduler.ApprovedDate = DateTime.Now;
                }
               else if(status == ApprovalStatus.Rejected)
                {
                    checkScheduler.IsAnswered = false;
                    checkScheduler.RejectedBy = Username;
                    checkScheduler.RejectedDate = DateTime.Now;


                    //EmailLogic for Rejection.
                   

                }
                checkScheduler.ApprovalStatus = status;
                _assessmentScheduling.Update(checkScheduler);
                return _assessmentScheduling.Save() > 0;


            }
            catch (Exception ex)
            {
                _loggerInfo.Error("Error::: ", "Message" + ex.Message + "  StackTrace:::" + ex.StackTrace + "  InnerException:::" + ex.InnerException);
                throw new Exception(ex.Message);
            }
        }


        public IList<AssessmentReviewVM> GetAllApprovalDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            // var result = GetDataTestsFromDbasePending(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount, username, approvalStatus);
            var result = GetAllAssessmentDbasePending(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount,ApprovalStatus.Approved );
        
            if (result == null)
            {

                result = new List<AssessmentReviewVM>();
                return result;
            }
            return result;
        }






        public IList<TestResultVM> GetAllTestDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string v, ApprovalStatus pending)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            // var result = GetDataTestsFromDbasePending(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount, username, approvalStatus);
            var result = new List<TestResultVM>();
            // GetDataTestsFromDbasePending(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount, v, pending);
            totalResultsCount = result.Count;
            filteredResultsCount = result.Count;
            if (result == null)
            {
                
              
                return result;
            }
            return result;
        }



        private List<AssessmentReviewVM> GetAllAssessmentDbasePending(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount, ApprovalStatus approvalStatus)
        {

            var record = _assessmentScheduling.GetAllIncluding(x => !x.IsDeleted &&x.ApprovalStatus==approvalStatus, false, x => x.User);

            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.User.DepartmentName.ToLower().Contains(searchBy.ToLower()));
    
            record = record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new AssessmentReviewVM
            {
                UserId = x.UserId,
                Department = x.User.DepartmentName,
                ApprovedBy = x.ApprovedBy,
                Username = x.User.Username,
                RejectedBy = x.RejectedBy,
                RejectedDate = x.RejectedDate,
                ApprovedDate = x.ApprovedDate
            }).ToList();
        }

        public async Task TestApproval(ApprovalVM model)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == model.Username.ToLower() && !x.IsDeleted);
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == model.QuestionId && !x.IsDeleted);
                if (entity == null)
                    throw new Exception("Record does not exist");

                entity.ApprovalStatus = model.ApprovalStatus;
                entity.ApprovedById = user.Id;
                _questionRepository.Update(entity);
                var updateEn = await _questionRepository.SaveAsync();
               // bool check = UpdateApproval(entity, user.Id, model.ApprovalStatus);
                if (updateEn == 0)
                    throw new Exception("Failed to update");
            }
            catch (Exception ex)
            {

                _loggerInfo.Error(ex, "Error on Saving  Category");

            }
        }
    }
}
