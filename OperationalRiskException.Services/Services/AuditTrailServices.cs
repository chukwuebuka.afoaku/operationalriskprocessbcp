﻿using NLog;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
   public class AuditTrailServices : IAuditTrailServices
    {
        private IRepository<AuditTrail> _auditRepo;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public AuditTrailServices()
        {
            _auditRepo = new Repository<AuditTrail>();
            
        }
        public void AddAudit(AuditTrail model)
        {
            try
            { 
                _auditRepo.Insert(model);
                var checker=  _auditRepo.Save();
                if (checker == 0)
                {
                    _loggerInfo.Error("Audit not saving", "Audit Failure");
                }
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Audit Failure");
               
            }

        }
    }
}
