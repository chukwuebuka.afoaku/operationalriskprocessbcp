﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.EmailServiceProvider;
using OperationalRiskException.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Services
{
    public class AssessmentPortalServices : IAssessmentPortalServices
    {
        private IRepository<AssessmentScheduling> _assessmentScheduler;
        private IRepository<AssessmentSetup> _assessmentSetup;
        private IRepository<User> _userRepo;
        private IRepository<Question> _questionRepo;
        private IRepository<QuestionCategory> _questionCategoryRepo;
        private IRepository<AssessmentRecord> _assessmentRecRepo;

        private IRepository<AnswerEntity> _answerRepo;
      //  private IRepository<CorrectivePlanSetup> _correctiveSetupRepo;

      //  private IRepository<CorrectivePlan> _correctivePlanRepo;
        private IRepository<SaveAssessmentByCategory> _saveAssessmentByCategory;


        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        private IEmailService _emailService;

        public AssessmentPortalServices( )
        {
            _assessmentScheduler = new Repository<AssessmentScheduling>();
            _userRepo = new Repository<User>();
            _assessmentSetup = new Repository<AssessmentSetup>();
            _questionRepo = new Repository<Question>();
            _questionCategoryRepo = new Repository<QuestionCategory>();
            _assessmentRecRepo = new Repository<AssessmentRecord>();
            _answerRepo = new Repository<AnswerEntity>();
            //  _correctiveSetupRepo = new Repository<CorrectivePlanSetup>();
            //  _correctivePlanRepo = new Repository<CorrectivePlan>();
            _saveAssessmentByCategory = new Repository<SaveAssessmentByCategory>();
            _emailService = new EmailService();
        }
        public async Task<ChampionDashboardVM> GetAllRecords(string username)
        {
            ChampionDashboardVM entity = new ChampionDashboardVM();
            var user =await  _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username);
            entity.UserId = user.Id;
            entity.Username = username;
            //
            var checkAVailableTest = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && x.UserId == user.Id  && !x.IsAnswered, false);
            var presentDate =DateTime.Now.Date.ToString("yyyy-MM-dd");
            var ff = checkAVailableTest.ToList();
           // var assessmentList =  _assessmentSetup.GetWithRawSql($"select * from AssessmentSetupsWHERE '{presentDate}' between StartDate AND EndDate   ").Where(x => checkAVailableTest.Select(y=>y.AssessmentSetupId).Contains(x.Id));
            var assessmentList =  _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups").Where(x => checkAVailableTest.Select(y=>y.AssessmentSetupId).Contains(x.Id));
            //result of test
            entity.IsTestAvailable = assessmentList != null ? true : false;
            entity.ListOfAvailableTest = assessmentList.Select(x => new AvailableAssessment {AssessmentName=x.AssessmentName, AssessmentScheduleId=x.Id }).ToList();
            entity.Scores = GetAllScoreUser(username);
            return entity;
        }

        public async Task<bool> Approved(int userId, int setupId)
        {
            try {
            var checkScheduler = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && x.IsAnswered && x.UserId == userId && x.AssessmentSetupId == setupId, true, x=>x.User).FirstOrDefault();
            if (checkScheduler == null)
                throw new Exception("There is no test scheduled for the user");

            checkScheduler.IsAnswered = true;
            checkScheduler.ApprovalStatus = ApprovalStatus.Approved;
            checkScheduler.ApprovedDate = DateTime.Now;
            checkScheduler.ApprovedBy = GetUsername();
         
            _assessmentScheduler.Update(checkScheduler);
                bool checkEmailStatus = _emailService.ApprovalEmail(checkScheduler);
                return await _assessmentScheduler.SaveAsync() > 0;

            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException + "Message:::" + exx.Message);


                return false;

            }

        }
        public async Task<bool> Rejected(int userId, int setupId)
        {
            var checkScheduler = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && x.IsAnswered && x.UserId == userId && x.AssessmentSetupId == setupId,true, y=>y.AssessmentSetup,z=>z.User).FirstOrDefault();
            if (checkScheduler == null)
                throw new Exception("There is no test scheduled for the user");
            //Sending Email here
            var sendCheck = SendRejectionEmail(checkScheduler);
            checkScheduler.IsAnswered = false;
            checkScheduler.ApprovalStatus = ApprovalStatus.Rejected;
            checkScheduler.RejectedDate = DateTime.Now;
            checkScheduler.RejectedBy = GetUsername();
            _assessmentScheduler.Update(checkScheduler);
            return await _assessmentScheduler.SaveAsync() > 0;

        }

        private bool SendRejectionEmail(AssessmentScheduling checkScheduler )
        {
            string FirstName = checkScheduler.User.Username.Split('.')[0].ToUpper();

            //string url = ConfigurationManager.AppSettings["rejectionTemplate"] as string;
            //string templatePath = HttpRuntime.AppDomainAppPath + url;


            var template = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["rejectionTemplate"] as string);

            string sender = ConfigurationManager.AppSettings["sender"] as string;
            string optRisk = ConfigurationManager.AppSettings["optRisk"] as string;
            var CC = optRisk.Split(',');
            string subject = "Assessment Re-taken";
            string body = GetMailBody(FirstName, checkScheduler.AssessmentSetup.StartDate.ToLongDateString(), checkScheduler.AssessmentSetup.EndDate.ToLongDateString(), template);
            string response = SendEmail(checkScheduler.User.Username, sender, subject, body, CC.ToArray());
            if (!(response == "Error"))
                return true;
            else
                return false;
        }
        public string SendEmail(string recipient, string sender, string subject, string body, string[] cc)
        {
            string result = "";
            try
            {

                EmailServiceReference.Service objPayRef = new EmailServiceReference.Service();
                //cc
                result = objPayRef.SendMail(recipient, sender, subject, body, cc);

                _loggerInfo.Info("SMS Message" + result);
                return result;
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException + "Message:::" + exx.Message,
                    "Assessment Error");
                return "Error";
            }

        }
        public static string GetMailBody(string Champion, string StartDate, string EndDate, string templatePath)
        {
            string body = "";
            try
            {
                using (var reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ChampionName}", Champion);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", EndDate);

            }
            catch (Exception ex)
            {
                return "";
            }

            if (string.IsNullOrEmpty(body))
            {
                return "";
            }
            return body;

        }



        public async Task<List<QuestionMgtVM>> GetAllRecordAllQuestions(string username, int AssSchId)
        {
            var httpRequest = HttpContext.Current.Request;
            var absolutePath = httpRequest.Url.AbsoluteUri;

            var domainPath = absolutePath.Replace(httpRequest.RawUrl, "") + httpRequest.ApplicationPath;
            var initialPath = ConfigurationManager.AppSettings["filepath"];
            try
            {
                //  QuestionCategories



                var questnMgt = new List<QuestionMgtVM>();
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username);
                if (user == null)
                    throw new Exception("User doesn't exist on the platform");

                var checkScheduler =  _assessmentScheduler.GetSingleRecordFilter(x =>  !x.IsDeleted && !x.IsAnswered && x.UserId==user.Id && x.AssessmentSetupId == AssSchId);
                if (checkScheduler == null)
                    throw new Exception("There is no test scheduled for the user");
                // Check Date range whether is between the range; Today
                var presentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                //var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups WHERE '{presentDate}' between StartDate AND EndDate").Where(x=>x.Id==AssSchId).FirstOrDefault();
                var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups").Where(x=>x.Id==AssSchId).FirstOrDefault();
                if (assessmentList == null)
                    throw new Exception("There is no avaliable");
                var allQuestionCategories = assessmentList.QuestionCategories.Split(',');
                foreach(var questCat in allQuestionCategories)
                {
                    var qc = int.Parse(questCat);
                    var qcDetails= _questionCategoryRepo.GetSingleRecordFilter(x => x.Id == qc);
                    var questDetails = _questionRepo.GetAllIncluding(x => x.QuestionCategoryId == qc && !x.IsDeleted && x.ApprovalStatus==ApprovalStatus.Approved, false);

                    var allQuestionCategory = new List<TestQuestionVM>();
                    foreach (var questn in questDetails)
                    {
                        allQuestionCategory.Add(new TestQuestionVM
                        {
                            CategoryName = qcDetails.CategoryName,
                            CategoryId = qcDetails.Id,
                            QuestionId=questn.Id,
                            AttachmentName = questn.AttachmentName,
                            AttachmentUpload = domainPath+ questn.AttachmentUpload
                          //  QuestionText=questn.QuestionText

                        });
                    }
                    questnMgt.Add(new QuestionMgtVM
                    {
                        CategoryName = qcDetails.CategoryName,
                        QuestionCategoryId = qcDetails.Id,
                        TestQuestion = allQuestionCategory,
                        Count= questDetails.Count()

                    }) ;
                    
                }

                return questnMgt;
            }
            catch(Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal Get Question for Users:::::");
                return new List<QuestionMgtVM>();
            }


        }
        private string GetUsername(string key = "UserRole")
        {
            try
            {
                var aCookie = System.Web.HttpContext.Current.Request.Cookies[key];
                var userRole = aCookie != null ? aCookie.Value : string.Empty;
                if (!string.IsNullOrWhiteSpace(userRole))
                {
                    var user = JsonConvert.DeserializeObject<RoleViewModel>(userRole);
                    return user.Username;
                }
                return "Anonymous";
            }
            catch (Exception exx)
            {
                return "Anonymous";
            }

        }
        public async Task<MainAnswerDto> GetAllSavedAssessment(string username, int QuestionCategoryId, int SetUpId)
        {
            var httpRequest = HttpContext.Current.Request;
            var absolutePath = httpRequest.Url.AbsoluteUri;

            var domainPath = absolutePath.Replace(httpRequest.RawUrl, "") + httpRequest.ApplicationPath;
            var initialPath = ConfigurationManager.AppSettings["filepath"];

            var list = new MainAnswerDto();
            try
            {
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                // 
                var allAnswers = _answerRepo.GetAllIncluding(x => x.AssessmentSetupId == SetUpId &&x.QuestionCategoryId == QuestionCategoryId && x.UserId==user.Id && !x.IsDeleted, false).ToList();

                list.answers = allAnswers.Select(x => new AnswersDto
                {
                    FilePath = domainPath + x.FilePath,
                    QuestionId = x.QuestionId,
                    
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex.ToString(), "AssessmentPortal GetAllAssessment::");
                return list;
            }
        }

     
        public async Task<bool> SaveFile(FileUploadVM questionVM, string username, int SetUpId)
        {
            try
            {
                
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                //Check for previous answer before saving
                var checkAnswer = await CheckPreviousAnswer(questionVM.QuestionCategoryId, user.Id, SetUpId, questionVM.QuestionId);
                if (checkAnswer == null)
                {
                    // If there is no answer before insert
                    var saveEntity = new AnswerEntity()
                    {
                        QuestionCategoryId=questionVM.QuestionCategoryId,
                        FilePath=questionVM.FilePath,
                        AssessmentSetupId= SetUpId,
                        UserId=user.Id,
                        QuestionId=questionVM.QuestionId
                    };
                    _answerRepo.Insert(saveEntity);
                    return _answerRepo.Save()>0;
                   
                }
                else
                {
                    // If there is an answer before Update
                    checkAnswer.FilePath = questionVM.FilePath;
                    _answerRepo.Update(checkAnswer);
                    return _answerRepo.Save() >0;
                }

            }
            catch (Exception ex)
            {
                _loggerInfo.Error("Message" + ex.Message+ "   StackTrace::::" + ex.StackTrace+ "  InnerException:::" + ex.InnerException, "AssessmentPortal Saved Record Error:::::");
                return false;
            }
        } 
          //  public async Task<bool> SaveFile(FileUploadVM questionVM, string username, int SetUpId)
        //{
        //    try
        //    {
        //        var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
        //        if (user == null)
        //            throw new Exception("User doesn't exist");
        //        var checkAnswer = await GetAssessmentRecord(questionVM.QuestionCategoryId, user.Id, SetUpId);
        //        if (checkAnswer == null)
        //        {
        //            var saveEntity = new AssessmentRecord()
        //            {
        //                QuestionCategoryId=questionVM.QuestionCategoryId,
        //                FilePath=questionVM.FilePath,
        //                AssessmentSetupId= SetUpId,
        //                UserId=user.Id
        //            };
        //            _assessmentRecRepo.Insert(saveEntity);
        //            return _assessmentRecRepo.Save()==1;
                   
        //        }
        //        else
        //        {
        //            checkAnswer.FilePath = questionVM.FilePath;
        //            _assessmentRecRepo.Update(checkAnswer);
        //            return _assessmentRecRepo.Save() == 1;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerInfo.Error(ex, "AssessmentPortal FileUpload for Users:::::");
        //        return false;
        //    }
        //}

        //public async Task<bool> UserCheck(string username, int AsschId)
        //{
        //    var user =await  _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
        //    if (user == null)
        //        throw new Exception("User doesn't exist");

        //}
      
        

           //public async Task<bool> SaveCorrectivePlan(SaveQuestionVM questionVM, string username, int SetUpId)
        //{
        //    int correctiveSetupId = 0;
        //    try
        //    {
        //        // File Upload before saving answer (Flow)

        //        var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
        //        if (user == null)
        //            throw new Exception("User doesn't exist");

        //        var checkCorrective = _correctiveSetupRepo.GetAllIncluding(x => x.AssessmentSetupId == SetUpId && x.UserId == user.Id, false).FirstOrDefault();
        //      if(checkCorrective != null)
        //            throw new Exception("Corrective Plan  existed before");
        //        var correctiveSetup = new CorrectivePlanSetup
        //        {
        //            AssessmentSetupId=SetUpId,
        //            UserId=user.Id,
                    

        //        };
        //        _correctiveSetupRepo.Insert(correctiveSetup);
        //        var check=_correctiveSetupRepo.Save();
        //        if (check == 1)
        //        {
        //            correctiveSetupId = correctiveSetup.Id;
        //            if (correctiveSetupId > 0)
        //            {
        //                foreach (var questn in questionVM.Questionlists)
        //                {
        //                    var answer = questn.value;
        //                    var questnNO = int.Parse(questn.name);
        //                    _correctivePlanRepo.Insert(new CorrectivePlan
        //                    {
        //                        CorrectiveText = answer,
        //                        QuestionId = questnNO,
        //                        CorrectivePlanSetupId = correctiveSetupId
        //                    });
        //                    _correctivePlanRepo.Save();
        //                }
        //            }
        //        }

        //            var checkScheduler = _assessmentScheduler.GetSingleRecordFilter(x => !x.IsDeleted && !x.HasCorrectivePlan && x.UserId == user.Id &&  x.AssessmentSetupId == SetUpId);
        //            if (checkScheduler != null)
        //            {
        //                checkScheduler.HasCorrectivePlan = true;
        //                _assessmentScheduler.Update(checkScheduler);
        //                _assessmentScheduler.Save();
        //            }
                

        //        return true;

        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerInfo.Error(ex, "AssessmentPortal CorrectivePlan Saves::");
        //        return false;
        //    }
        //}
        //public List<CorrectivePlanDisplayVM> getAllCorrectivePlan( int userId, int SetUpId)
        //{
        //    var lists = new List<CorrectivePlanDisplayVM>();
        //    try
        //    {
        //        // File Upload before saving answer (Flow)

        //        var checkCorrective = _correctiveSetupRepo.GetAllIncluding(x => x.AssessmentSetupId == SetUpId && x.UserId == userId, false).FirstOrDefault();
        //        if (checkCorrective == null)
        //            throw new Exception("Corrective Plan  not exist ");
        //        var getCorrectivePlans = _correctivePlanRepo.GetAllIncluding(x => x.CorrectivePlanSetupId == checkCorrective.Id, false, y=>y.Question).ToList();
        //        var results = getCorrectivePlans.Select(x => new CorrectivePlanDisplayVM {
        //            CorrectivePlan=x.CorrectiveText,
        //            QuestionTest=x.Question.QuestionText
        //        }).ToList();
        //        lists = results;
        //        return lists;


        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerInfo.Error(ex, "AssessmentPortal CorrectivePlan Display::");
        //        return lists;
        //    }
        //}
        private async Task<AssessmentRecord> GetAssessmentRecord(int questCatId, int userId, int SetUpId)
        {
         var record=   await _assessmentRecRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted
                  && x.QuestionCategoryId == questCatId && x.UserId == userId && x.AssessmentSetupId==SetUpId);
            return record;
        }
        private async Task<AnswerEntity> CheckPreviousAnswer(int questCatId, int userId, int SetUpId, int QuestionId)
        {
            var record = await _answerRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted
                    && x.QuestionCategoryId == questCatId && x.UserId == userId && x.AssessmentSetupId == SetUpId&& x.QuestionId==QuestionId);
            return record;
        }
        private  List<AssessmentRecord> GetAssessmentRecords( int userId, int SetUpId)
        {
            var record =  _assessmentRecRepo.GetAllIncluding(x => !x.IsDeleted
                     && x.UserId == userId && x.AssessmentSetupId == SetUpId, false, y=>y.QuestionCategory);
            return record.ToList();
        }

        public Task<bool> SaveCorrectivePlan(SaveQuestionVM questionVM, string username, int SetUpId)
        {
            throw new NotImplementedException();
        }

        public List<CorrectivePlanDisplayVM> getAllCorrectivePlan(int userId, int SetUpId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> SaveAssessment(SaveQuestionVM questionVM, string username, int SetUpId)
        {
            throw new NotImplementedException();
        }

        public List<ScoreVM> GetAllScores()
        {
            throw new NotImplementedException();
        }

        public List<ScoreVM> GetAllScoreUser(string username)
        {
            //score
            var score = new List<ScoreVM>();
            return score;
        }

      
        public List<MainAnswerDto> GetAllSavedAssessments(int userId, int SetUpId)
        {
            var httpRequest = HttpContext.Current.Request;
            var absolutePath = httpRequest.Url.AbsoluteUri;

            var domainPath = absolutePath.Replace(httpRequest.RawUrl, "") + httpRequest.ApplicationPath;
            var initialPath = ConfigurationManager.AppSettings["filepath"];

            //throw new NotImplementedException();
            var answers = new List<MainAnswerDto>();

            var allAnswers = _answerRepo.GetAllIncluding(x => !x.IsDeleted && x.AssessmentSetupId==SetUpId &&x.UserId==userId, false, x=>x.QuestionCategory, x=>x.Question).ToList();
            var groupCategories = allAnswers.GroupBy(x =>new  { x.QuestionCategoryId, x.QuestionCategory.CategoryName});
            foreach (var x in groupCategories)
            {
                //
                var aSingleAnswerCat = new MainAnswerDto()
                {
                    CategoryName = x.Key.CategoryName,
                    CategoryId = x.Key.QuestionCategoryId,
                    answers = allAnswers.Where(t=>t.QuestionCategoryId==x.Key.QuestionCategoryId).Select(k => new AnswersDto { AnswerPath = domainPath+ k.FilePath, QuestionFilePath = domainPath  + k.Question.AttachmentUpload, AttachmentName=k.Question.AttachmentName }).ToList()
                };
                answers.Add(aSingleAnswerCat);
            }

      

                return answers;
            
        }
        public void UpdateSomething()
        {
            var checkScheduler = _assessmentScheduler.GetAllRecordsFilters(x => !x.IsDeleted && x.IsAnswered && x.AssessmentDateCompleted==null).ToList();
            foreach(var item in checkScheduler)
            {
                var ansInfo = _answerRepo.GetAllRecordsFilters(x => !x.IsDeleted && x.UserId == item.UserId && x.AssessmentSetupId == item.AssessmentSetupId).LastOrDefault();
                item.AssessmentDateCompleted = ansInfo.CreatedDate;
                _assessmentScheduler.Update(item);
                _assessmentScheduler.Save();
            }


        }
        public async Task<bool> SaveFinalAssessment( string username, int schedulerId)
        {
            try {
            var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username);
            if (user == null)
                throw new Exception("User doesn't exist on the platform");

            var checkScheduler = _assessmentScheduler.GetSingleRecordFilter(x => !x.IsDeleted && !x.IsAnswered && x.UserId == user.Id && x.AssessmentSetupId == schedulerId);
            if (checkScheduler == null)
                throw new Exception("There is no test scheduled for the user");

                checkScheduler.IsAnswered = true;
                checkScheduler.AssessmentDateCompleted = DateTime.Now;
                checkScheduler.ApprovalStatus = ApprovalStatus.Pending;
                _assessmentScheduler.Update(checkScheduler);
               return  _assessmentScheduler.Save()> 0;


            }
            catch (Exception ex)
            {
                _loggerInfo.Error("Error::: ", "Message" + ex.Message + "  StackTrace:::" + ex.StackTrace + "  InnerException:::" + ex.InnerException);
                throw new Exception(ex.Message);
            }
        }
    }
}
