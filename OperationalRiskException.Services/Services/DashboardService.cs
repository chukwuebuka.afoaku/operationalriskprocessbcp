﻿
using System;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using NLog;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Services.Interfaces;

namespace OperationalRiskException.Services.Services
{
    public class DashboardService :IDashboardService
    {
        string connstring = ConfigurationManager.ConnectionStrings["OperationalRiskBCPContext"].ConnectionString;

        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

        public DashboardService()
        {

        }



        public DashboardDto DashBoard()
        {
            string proc = ConfigurationManager.AppSettings["Sp_DashboardRecord"].ToString();
            var dashboardRecord = new DashboardDto();
            using (SqlConnection sql = new SqlConnection(connstring))
            {
                try
                {


                    SqlCommand cmd = new SqlCommand(proc, sql);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@NoOfCampions", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@NotYetTakenAssessments", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@TakenAssessments", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sql.Open();
                    cmd.ExecuteNonQuery();
                    dashboardRecord.NoOfChampions=  Convert.ToInt32(cmd.Parameters["@NoOfCampions"].Value);
                    dashboardRecord.NotYetTakenAssessments = Convert.ToInt32(cmd.Parameters["@NotYetTakenAssessments"].Value);
                    dashboardRecord.TakenAssessments = Convert.ToInt32(cmd.Parameters["@TakenAssessments"].Value);
                }
                catch (Exception ex)
                {
                    _loggerInfo.Error(ex.ToString(), "SQL QUERY");
                }
                finally
                {
                    sql.Close();
                }
            }

            return dashboardRecord;


        }
    }

  
}
