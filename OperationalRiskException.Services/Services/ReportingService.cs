﻿using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.ReportRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OperationalRiskException.Core.ViewModels.ReportDisplays;

namespace OperationalRiskException.Services.Services
{
  public interface IReportingService
    {
         ReportResponce GetApprovalWorkflowDetails();
        List<ReportModel> ViewResultsByDepartment(string dept);
    }
    public class ReportingService : IReportingService
    {
       private IReportingRepo _reportingRepo;
        public ReportingService()
        {

            _reportingRepo = new ReportingRepo();
        }

        public ReportResponce GetApprovalWorkflowDetails()
        {
            var report = _reportingRepo.GetApprovalWorkflowDetails();
            return report;
        }

        public List<ReportModel> ViewResultsByDepartment(string dept)
        {
            var report = _reportingRepo.ViewResultsByDepartment(dept);
            return report;
        }
    }
}
