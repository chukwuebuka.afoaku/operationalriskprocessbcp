﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Data.EscalationRepo;
using OperationalRiskException.Services.EmailServiceProvider;
using OperationalRiskException.Services.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
    public class EscalationService : IEscalationService
    {


        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();


        private IEmailService _emailService;

        private IEscalationRepository _escalationRepository;
        private IUserService _userService;

        public EscalationService(
           )
        {
            // _issueDetailRepo = issueDetailRepo;
            _emailService = new EmailService();
            _escalationRepository = new EscalationRepository();
            _userService = new UserService();

        }
        public async Task FirstEscalationImplementation()
        {
            try
            {
                var listPendingEscalatn = _escalationRepository.PendingEscalation();
                var updatedList = _escalationRepository.InsertFirstEscalationRecords(listPendingEscalatn);
                _loggerInfo.Info("UpdatedList::: " + updatedList.ToString());
                foreach (var pendingItem in listPendingEscalatn)
                {

                    var escalationDetails = await _userService.GetHRStaff(pendingItem.Username);
                    //Role Check


                    _loggerInfo.Info("EscalationDetails:::: " + JsonConvert.SerializeObject(escalationDetails));
                    if (escalationDetails != null)
                    {
                        if (escalationDetails.SupervUser == null)
                        {
                            _loggerInfo.Info("NO Supervisor:::: from the ad service ");
                            

                            var user = _escalationRepository.GetSTAFF_USER(pendingItem.Username);
                            if (user == null)
                            {
                                // return ContinuousEscalationImplementation;
                                continue;
                            }
                            escalationDetails.SupervUser = user.SupervUser;
                            if (escalationDetails.SupervUser != null)
                            {
                                var emailSenderCheck = _emailService.EscalationEmail(escalationDetails.SupervUser, pendingItem);
                                _loggerInfo.Info("emailSenderCheck::: " + emailSenderCheck.ToString());
                                if (emailSenderCheck)
                                {
                                  //  var checkRecordSaved = _escalationRepository.SaveFirstEscalation(escalationDetails.SupervUser, escalationDetails.SupervGrade, pendingItem);
                                    //_loggerInfo.Info("checkRecordSaved::: " + checkRecordSaved.ToString());
                                }
                            }


                            //_loggerInfo.Info("NO Supervisor:::: ");
                        }
                        else
                        {
                            var emailSenderCheck = _emailService.EscalationEmail(escalationDetails.SupervUser, pendingItem);
                            _loggerInfo.Info("emailSenderCheck::: " + emailSenderCheck.ToString());
                            if (emailSenderCheck)
                            {
                             //var checkRecordSaved = _escalationRepository.SaveFirstEscalation(escalationDetails.SupervUser, escalationDetails.SupervGrade, pendingItem);
                               // _loggerInfo.Info("checkRecordSaved::: " + checkRecordSaved.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Error FirstEscalationImplementation", exx.ToString());
            }




        }

        public async Task ContinuousEscalationReviewed()
        {
            try
            {

                var listPendingEscalatn = _escalationRepository.OnEscalationList();
                Log.Information("ContinuousEscalationReviewed Count::: " + listPendingEscalatn.Count.ToString());
                foreach (var pendingItem in listPendingEscalatn)
                {
                    var champItem = new ChampionEscalationDTO()
                    {
                        Username = pendingItem.Username,

                        
                    };

                    var emailSenderCheck = _emailService.EscalationEmail(pendingItem.SupervisorEmail, champItem);
                    Log.Information("emailSenderCheck::: " + emailSenderCheck.ToString());
                    await Task.FromResult<object>(null);

                }

                }
            catch (Exception exx)
            {
                Log.Debug(exx.ToString());
                Log.Error("Error ContinuousEscalationReviewed", exx.ToString());
            }




        }


        public async Task SoftWarningEscalation()
        {
            try
            {
                var listPendingEscalatn = _escalationRepository.WarningEscalation();
                Log.Information("SoftWarningEscalation Count:::: "+ listPendingEscalatn.Count);

                foreach (var pendingItem in listPendingEscalatn)
                {
                    Log.Information("Item on warning :::: " + JsonConvert.SerializeObject(pendingItem));

                    var escalationDetails = await _userService.GetHRStaff(pendingItem.Username);
                    //Role Check


                    Log.Information("EscalationDetails:::: " + JsonConvert.SerializeObject(escalationDetails));
                    if (escalationDetails != null)
                    {
                        if (escalationDetails.SupervUser == null)
                        {
                            Log.Information("NO Supervisor:::: from the ad service ");
                            var user = _escalationRepository.GetSTAFF_USER(pendingItem.Username);
                            if (user == null)
                            {
                                continue;
                            }
                            escalationDetails.SupervUser = user.SupervUser;
                            if (escalationDetails.SupervUser != null)
                            {
                                var emailSenderCheck = _emailService.SoftWarning(escalationDetails.SupervUser, pendingItem);
                                Log.Information("emailSenderCheck::: " + emailSenderCheck.ToString());
                            }
                        }
                        else
                        {
                            var emailSenderCheck = _emailService.SoftWarning(escalationDetails.SupervUser, pendingItem);
                            Log.Information("emailSenderCheck::: " + emailSenderCheck.ToString());

                        }
                    }
                }
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Error FirstEscalationImplementation", exx.ToString());
            }




        }






        public async Task ContinuousEscalationImplementation()
        {
            try
            {
                var listPendingEscalatn = _escalationRepository.OnEscalationList();
                Log.Information("ContinuousEscalationImplementation Count::: " + listPendingEscalatn.Count.ToString());
                foreach (var pendingItem in listPendingEscalatn)
                {
                    //
                    var escalationDetails = await _userService.GetHRStaff(pendingItem.SupervisorEmail);
                    _loggerInfo.Info("EscalationDetails " + JsonConvert.SerializeObject(escalationDetails));
                    if (escalationDetails.SupervUser != null)
                    {
                        var champItem = new ChampionEscalationDTO()
                        {
                            Username = pendingItem.Username,
                            

                        };
                        var emailSenderCheck = _emailService.EscalationEmail(escalationDetails.SupervUser, champItem);
                        _loggerInfo.Info("emailSenderCheck " + emailSenderCheck.ToString());
                        if (emailSenderCheck)
                        {
                            var checkRecordSaved = _escalationRepository.SaveOtherEscalation(escalationDetails.SupervUser, escalationDetails.StaffGrade, pendingItem);
                            _loggerInfo.Info("checkRecordSaved::: " + checkRecordSaved.ToString());

                        }

                    }
                }
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Error FirstEscalationImplementation", exx.ToString());
            }




        }
    }
}

