﻿using NLog;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.Context;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Data.UnitOfWork;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Services
{
    public class QuestionService: IQuestionService
    {


        private IRepository<Question> _questionRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        private IRepository<BatchUpload> _batchRepo;
        private IRepository<Approval> _approvalRepo;
        private IRepository<User> _userRepository;
        private IUnitOfWork _unitOfWork;

        private readonly OperationalRiskContext context;
        public QuestionService()
        {
            context = new OperationalRiskContext();
            _questionRepository = new Repository<Question>();
            _approvalRepo =new Repository<Approval>();
            _batchRepo = new Repository<BatchUpload>();
            _userRepository = new Repository<User>();
            _unitOfWork = new UnitOfWork(new OperationalRiskContext());
        }
        private bool AddApprovalWorkFlow(Question entity, int userId, ApprovalStatus approvalStatus)
        {
            try
            {
                var entityApproval = new Approval
                {
                    ApprovalStatus = approvalStatus,
                    QuestionHistory = entity.QuestionText,
                    QuestionId = entity.Id,
                    IsActive = true,
                    QuestionCreatedById = userId,
                };

                _approvalRepo.Insert(entityApproval);
                return _approvalRepo.Save() >0;
            }
            catch(Exception ex)
            {
                _loggerInfo.Error("AddApprovalWorkFlow::: StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");

                throw new Exception(ex.Message);
            }
        }
        private bool UpdateQuestionApproval(Question entity, int userId, ApprovalStatus approvalStatus)
        {
            try { 
            var allQuestionApproval = _approvalRepo.GetSingleRecordFilter(x => !x.IsDeleted && x.IsActive  && x.QuestionId==entity.Id);
            if (allQuestionApproval != null)
            {
               
                allQuestionApproval.IsActive = false;
                _approvalRepo.Update(allQuestionApproval);
                _approvalRepo.Save();
            }
            var entityApproval = new Approval
            {
                ApprovalStatus = approvalStatus,
                QuestionHistory = entity.QuestionText,
                QuestionId = entity.Id,
                IsActive = true,
                QuestionCreatedById = userId,
            };

            _approvalRepo.Insert(entityApproval);
            return _approvalRepo.Save() > 0;
        }
            catch (Exception ex)
            {
                _loggerInfo.Error("UpdateQuestion Approval::: StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");


                return false;
            }
}
        private bool UpdateApproval(Question entity, int userId, ApprovalStatus approvalStatus)
        {
            try { 
            var approval = _approvalRepo.GetSingleRecordFilter(x => !x.IsDeleted && x.IsActive && x.QuestionId == entity.Id);
            if (approval != null)
            {
                approval.ApprovalStatus = approvalStatus;
                approval.IsActive = true;
                _approvalRepo.Update(approval);

                return _approvalRepo.Save() > 0;
            }
                return false;
          }
            catch (Exception ex)
            {
                _loggerInfo.Error("UpdateApproval StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");


                return false;
            }


       }

        //--- chuks 

        private bool RejectApproval(Question entity, int userId, ApprovalStatus approvalStatus)
        {
            try
            {
                var approval = _approvalRepo.GetSingleRecordFilter(x => !x.IsDeleted && x.IsActive && x.QuestionId == entity.Id);
                if (approval != null)
                {
                    approval.ApprovalStatus = approvalStatus;
                    approval.IsActive = true;
                    _approvalRepo.Delete(approval);

                    return _approvalRepo.Save() > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("RejectApproval StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");


                return false;
            }


        }


        public async Task<bool> AddQuestion(QuestionVM model)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == model.CreateUser.ToLower() && !x.IsDeleted);
                var entity = new Question
                {
                    Id = model.QuestionId,
                    QuestionText = model.QuestionText,
                    QuestionCategoryId = model.CategoryId,
                    QuestionCreatedOrModifiedById = user.Id,
                    AttachmentName = model.AttachmentName,
                    AttachmentUpload = model.AttachmentUpload,
                    ApprovalStatus=ApprovalStatus.Pending,        
                };
                _questionRepository.Insert(entity);
                var status = await _questionRepository.SaveAsync();
                bool check = AddApprovalWorkFlow(entity, user.Id, ApprovalStatus.Pending);
                return status>=1 && check;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("AddQuestion StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");


                return false;
            }

        }

        public async Task UpdateQuestion(QuestionVM model)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == model.CreateUser.ToLower() && !x.IsDeleted);
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == model.QuestionId && !x.IsDeleted);
                if (entity == null)
                    throw new Exception("Record does not exist");
               // entity.QuestionText = model.QuestionText;
                entity.QuestionCategoryId = model.CategoryId;
                //Added the attachment name and upload
                entity.AttachmentName = model.AttachmentName;
               // entity.AttachmentUpload = model.AttachmentUpload;
                entity.ApprovalStatus = ApprovalStatus.Pending;
                _questionRepository.Update(entity);
                var updateEn = await _questionRepository.SaveAsync();
              bool check=  UpdateQuestionApproval(entity, user.Id, ApprovalStatus.Pending);
                if (updateEn == 0)
                    throw new Exception("Failed to update");
            }
            catch (Exception ex)
            {

                _loggerInfo.Error("UpdateQuestion StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");


            }
        }

        public async Task<QuestionVM> GetQuestion(int Id)
        {
            var entity = await _questionRepository.GetByIdAsync(Id);
            var qc = new QuestionVM
            {
              //  QuestionText = entity.QuestionText,
                AttachmentName = entity.AttachmentName,
                AttachmentUpload = entity.AttachmentUpload,
                CategoryId = (int)entity.QuestionCategoryId,
                QuestionId = entity.Id
            };
            return qc;
        }


        public IEnumerable<Question> GetAllQuestion()
        {
            //var entity = await _questionRepository.GetAllAsync();
            //return entity
            var query = from v in context.Questions
                        select v;
            return query;
        }




        public async Task<int> DeleteQuestion(int questionId)
        {
            try
            {
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == questionId && !x.IsDeleted);
                if (entity != null)
                {
                    _questionRepository.Delete(entity);
                    var deleteEn = await _questionRepository.SaveAsync();
                    return deleteEn;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("DeleteQuestion StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");
                _unitOfWork.RollBack();
                throw new Exception("Invalid records");

            }
        }
        public IList<QuestionVM> GetAllQuestionDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, ApprovalStatus approvalStatus=ApprovalStatus.Approved)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataQuestionsFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount, approvalStatus);
            if (result == null)
            {
                result = new List<QuestionVM>();
                return result;
            }
            return result;
        }
        public IList<QuestionVM> GetAllQuestionDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string username, ApprovalStatus approvalStatus = ApprovalStatus.Pending)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataQuestionsFromDbasePending(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount, username, approvalStatus);
            if (result == null)
            {
                result = new List<QuestionVM>();
                return result;
            }
            return result;
        }
        private List<QuestionVM> GetDataQuestionsFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount, ApprovalStatus approvalStatus=ApprovalStatus.Approved)
        {
            var httpRequest = HttpContext.Current.Request;
            var absolutePath = httpRequest.Url.AbsoluteUri;

            var domainPath = absolutePath.Replace(httpRequest.RawUrl, "") + httpRequest.ApplicationPath;
            var initialPath = ConfigurationManager.AppSettings["filepath"];
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var record = _questionRepository.GetAllIncluding(x => !x.IsDeleted &&(x.ApprovalStatus== approvalStatus), false, x => x.QuestionCategory);

            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.QuestionText.ToLower().Contains(searchBy.ToLower()) ||
                x.QuestionCategory.CategoryName.ToLower().Contains(searchBy.ToLower())
                );
            //SortBy
            record = sortBy == "QuestionText" ? record.OrderBy<Question>(x => x.QuestionText, sortDir) :
                   sortBy == "CategoryName" ? record.OrderBy<Question>(x => x.QuestionCategory.CategoryName, sortDir) :
                  record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new QuestionVM
            {
                QuestionText = x.QuestionText,
                QuestionId = x.Id,
                CategoryId = (int)x.QuestionCategoryId,
                CategoryName = x.QuestionCategory.CategoryName,
                AttachmentName = x.AttachmentName,
                AttachmentUpload = 
                domainPath+x.AttachmentUpload

            }).ToList();
        }

        private List<QuestionVM> GetDataQuestionsFromDbasePending(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount, string username, ApprovalStatus approvalStatus = ApprovalStatus.Pending)
        {
            var httpRequest = HttpContext.Current.Request;
            var absolutePath = httpRequest.Url.AbsoluteUri;

            var domainPath = absolutePath.Replace(httpRequest.RawUrl, "") + httpRequest.ApplicationPath;
            var initialPath = ConfigurationManager.AppSettings["filepath"];
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var user = _userRepository.GetSingleRecordFilter(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
            var record = _questionRepository.GetAllIncluding(x => !x.IsDeleted && x.QuestionCreatedOrModifiedById != user.Id && (x.ApprovalStatus == approvalStatus), false, x => x.QuestionCategory);

            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.QuestionText.ToLower().Contains(searchBy.ToLower()) ||
                x.QuestionCategory.CategoryName.ToLower().Contains(searchBy.ToLower())
                );
            //SortBy
            record = sortBy == "QuestionText" ? record.OrderBy<Question>(x => x.QuestionText, sortDir) :
                   sortBy == "CategoryName" ? record.OrderBy<Question>(x => x.QuestionCategory.CategoryName, sortDir) :
                  record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new QuestionVM
            {
                QuestionText = x.QuestionText,
                QuestionId = x.Id,
                CategoryId = (int)x.QuestionCategoryId,
                CategoryName = x.QuestionCategory.CategoryName,
                AttachmentName = x.AttachmentName,
                AttachmentUpload =domainPath  + x.AttachmentUpload


            }).ToList();
        }

        public int ExcelUpload(QuestionVM question, Stream FileUpload, out int shetCount,string username)
        {
            int sheet = 0;
            shetCount = 0;
            try
            {
                Guid guid = Guid.NewGuid();
                string BatchNo = guid.ToString();
                sheet = GetAllRecords(FileUpload, BatchNo, question.CategoryId, out int sheetCount, username);
                shetCount = sheetCount;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("ExcelUpload StackTrace" + ex.StackTrace + "InnerException::::" + ex.InnerException + "Message" + ex.Message, "Error on  Question Upload");

            }
            return sheet;
        }
        private int GetAllRecords(Stream FileUpload, string batchNo, int categoryId, out int sheetCount, string username)
        {
            int rowSave = 0;
            IDictionary<int, string> mapper = new Dictionary<int, string>();
            List<Question> records = new List<Question>();
            ISheet sheet;
            XSSFWorkbook hssfwb = new XSSFWorkbook(FileUpload);
            sheet = hssfwb.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            sheetCount = sheet.LastRowNum;
            //getting the header
            for (int j = 0; j < cellCount; j++)
            {
                ICell cell = headerRow.GetCell(j);
                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                mapper.Add(j, cell.ToString());
                //headers.Add(cell.ToString());
            }
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                var entity = new Question();
                IRow row = sheet.GetRow(i);
                if (row == null) continue;
                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (row.GetCell(j) != null)
                    {
                        entity.QuestionText = row.GetCell(j).ToString();
                    }
                }
                entity.BatchNo = batchNo;
                entity.QuestionCategoryId = categoryId;
                var questn = AddQuestionUpload(entity, username);

                rowSave += questn;

            }
            var batchEntity = new BatchUpload { BatchNo = batchNo,
            CategoryId=categoryId,
            NumberOfRecords=rowSave
            };
            _batchRepo.Insert(batchEntity);
            _batchRepo.Save();

            return rowSave;
        }
        private int AddQuestionUpload(Question model, string username)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == username && !x.IsDeleted);
                model.QuestionCreatedOrModifiedById = user.Id;
                _questionRepository.Insert(model);
                var status =  _questionRepository.Save();
                bool check = AddApprovalWorkFlow(model, user.Id, ApprovalStatus.Pending);
                return (status > 0 && check)?1:0;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("File StackTrace"+ex.StackTrace+"InnerException::::"+ex.InnerException+ "Message" + ex.Message, "Error on  Question Upload");
                return 0;
            }

        }

        public async Task QuestionApproval(ApprovalVM model)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == model.Username.ToLower() && !x.IsDeleted);
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == model.QuestionId && !x.IsDeleted);
                if (entity == null)
                    throw new Exception("Record does not exist");
           
                entity.ApprovalStatus = model.ApprovalStatus;
                entity.ApprovedById = user.Id;
                _questionRepository.Update(entity);
                var updateEn = await _questionRepository.SaveAsync();
                bool check = UpdateApproval(entity, user.Id, model.ApprovalStatus);
                if (updateEn == 0)
                    throw new Exception("Failed to update");
            }
            catch (Exception ex)
            {

                _loggerInfo.Error(ex, "Error on Saving  Category");

            }
        }

        public  async Task QuestionReject(ApprovalVM model)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Username.ToLower() == model.Username.ToLower() && !x.IsDeleted);
                var entity = _questionRepository.GetSingleRecordFilter(x => x.Id == model.QuestionId && !x.IsDeleted);
                if (entity == null)
                    throw new Exception("Record does not exist");

                entity.ApprovalStatus = model.ApprovalStatus;
                entity.ApprovedById = user.Id;
                _questionRepository.Delete(entity);
                var updateEn = await _questionRepository.SaveAsync();
                bool check = RejectApproval(entity, user.Id, model.ApprovalStatus);
                if (updateEn == 0)
                    throw new Exception("Failed to update");
            }
            catch (Exception ex)
            {

                _loggerInfo.Error(ex, "Error on Saving  Category");

            }
        }


        public async Task<bool> GetValidation(string questionText)
        {
            var entity = await _questionRepository.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.QuestionText.ToLower() == questionText.ToLower());

            return entity == null;

        }



        // public IQueryable<T>
    }
}
