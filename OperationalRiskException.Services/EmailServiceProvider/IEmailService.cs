﻿using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;

namespace OperationalRiskException.Services.EmailServiceProvider
{
    public interface IEmailService
    {
        bool EscalationEmail(string supervUser, Core.Dtos.ChampionEscalationDTO pendingItem);
        bool SoftWarning(string supervUser, ChampionEscalationDTO pendingItem);

        bool ApprovalEmail(AssessmentScheduling model);

        bool RejectionEmail(AssessmentScheduling model);
    }
}