﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OperationalRiskException.Services.Interfaces
{
    public interface ITestResultService
    {
        bool ApproveOrRejectAssessment(int schedulerId, ApprovalStatus status, string Username);
        IList<AssessmentReviewVM> GetAllApprovalDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount);
        IList<TestResultVM> GetAllTestDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string v, ApprovalStatus pending);
        List<AssessmentReviewVM> PendingAssessments(DateTime startDate, DateTime endDate);
        List<AssessmentReviewVM> RejectedAssessments();
        List<AssessmentReviewVM> ApprovedAssessments();
        Task TestApproval(ApprovalVM model);
        List<AssessmentReport> ReportingGenerator();
    }
}
