﻿using OperationalRiskException.Escalation.Scheduler;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperationalRiskException.Escalation.App_Start
{
    public class QuartzStartup
    {
        public static void Start()
        {
            // construct a scheduler factory


            StdSchedulerFactory factory = new StdSchedulerFactory();

            // get a scheduler
            IScheduler scheduler = factory.GetScheduler();
            scheduler.Start();

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<SchedulingImplementation>()
                .WithIdentity("myJob", "group1")
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("myTrigger", "group1")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(12)
                    .RepeatForever())
            .Build();


            IJobDetail job2 = JobBuilder.Create<OtherSchedulingImplementation>()
               .WithIdentity("myJob2", "group2")
               .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger2 = TriggerBuilder.Create()
                .WithIdentity("myTrigger2", "group2")
              
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(13)
                    .RepeatForever())
            .Build();

            scheduler.ScheduleJob(job, trigger);
            scheduler.ScheduleJob(job2, trigger2);

        }


    }
}