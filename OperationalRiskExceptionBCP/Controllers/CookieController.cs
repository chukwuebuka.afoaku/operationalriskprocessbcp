﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class CookieController : Controller
    {
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

        public void AddToCookie(RoleViewModel roleView)
        {
            var sessionTimeout = Convert.ToDouble(ConfigurationManager.AppSettings["sessionTimeout"]);
            var userCookies = new System.Web.HttpCookie("UserRole")
            {
                Value = Utility.Base64Encode( JsonConvert.SerializeObject(roleView)),
                Expires = DateTime.Now.AddMinutes(sessionTimeout)
            };
            //userCookies.Secure = true;
            //userCookies.HttpOnly = true;
            System.Web.HttpContext.Current.Response.Cookies.Add(userCookies);

        }

        public string ReadFromCookie(string key)
        {
            var aCookie = System.Web.HttpContext.Current.Request.Cookies[key];
            var userRole = aCookie != null ? aCookie.Value : string.Empty;
            if (!string.IsNullOrWhiteSpace(userRole))
            {
                string userObject= Utility.Base64Decode(userRole);

                AddToCookie(JsonConvert.DeserializeObject<RoleViewModel>(userObject));
                userRole = userObject;
            }
            _loggerInfo.Info("UserRole"+userRole);
            return userRole;
        }
    }
}