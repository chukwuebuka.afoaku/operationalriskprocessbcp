﻿using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OperationalRiskException.Core.ViewModels.ReportDisplays;

namespace OperationalRiskException.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        private IReportingService _report;
        private readonly ITestResultService _testresultservice;
        public ReportController()
        {
            _report = new ReportingService();
            _testresultservice = new TestResultService();
        }
       
        public ActionResult Index()
        {
            var data = _testresultservice.ReportingGenerator();
            return View(data);
        }
        [HttpGet]
        public ActionResult ViewByDept(string dept)
        {
            ViewBag.Dept = dept;
            var data = _report.ViewResultsByDepartment(dept);
            return View(data);
        }

        [HttpGet]
        public ActionResult DownnloadReport()
        {
            var model = _testresultservice.ReportingGenerator();
            var excel = GenerateToExcel.GenerateExcel<AssessmentReport>(model);

            Response.Clear();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "tpms_dict.xls"));
            Response.BinaryWrite(excel.GetBuffer());

            Response.End();

            return Content("Downloaded");

        }



        [HttpPost]
        public ActionResult Index(ReportRequest _params)
        {
            var data = _report.GetApprovalWorkflowDetails();
            return View(data);
        }

    }
}