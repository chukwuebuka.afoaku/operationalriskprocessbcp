﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    [CustomAuthorize(UserRoleEnum.Champion)]
   
    public class AssessmentPortalController : Controller
    {
        // GET: AssessmentPortal
        private IAssessmentPortalServices _assessmentPortalServices;
        private readonly IQuestionCategoryService _questionCategory;
        private readonly IQuestionService _question;

        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

        public AssessmentPortalController()
        {
            _questionCategory = new QuestionCategoryService();
            _question = new QuestionService();
            _assessmentPortalServices = new AssessmentPortalServices();
        }

        [LogActionFilter]
        public async Task<ActionResult> Dashboard()
        {
            var user = new CookieController().ReadFromCookie("UserRole");
            var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
            string username = deUser.Username;

            var result = await _assessmentPortalServices.GetAllRecords(username);
            return View(result);
        }
        [LogActionFilter]
        public ActionResult Records()
        {
            var totalScore = _assessmentPortalServices.GetAllScores();
            return View(totalScore);
        }
      
       
    
        [LogActionFilter]
        
        public ActionResult TakeTest( int AssSchId=0)
        {
            if (AssSchId == 0)
                return RedirectToAction("Login","Account");

            //Added this
           // ViewBag.Categories = _questionCategory.GetAllQuestionCategories();



            var assSchId = new HttpCookie("AssSchId")
            {
                Value = AssSchId.ToString(),
                Expires = DateTime.Now.AddHours(234)
            };
            Response.Cookies.Add(assSchId);
     
            
            return View();
        }
        

        [HttpPost]
        public async Task<ActionResult> TakeTest(TestQuestionVM model, HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null && FileUpload.ContentLength > 0)
                try
                {
                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Doc/");
                    if (!Directory.Exists(targetpath))
                    {
                        Directory.CreateDirectory(targetpath);
                    }
                    string absolutePath = "BcpUpload" + DateTime.Now.ToString("MMddyyyyHHmmss") + filename.Trim();
                    string pathToExcelFile = targetpath + absolutePath;
                    //string domainPath = ConfigurationManager.AppSettings["DomainName"];
                    FileUpload.SaveAs(pathToExcelFile);
                    model.AttachmentUpload =   "/Doc/" + absolutePath;
                    //var checkSave = await _assessmentPortalServices.SaveFile(model);
                    //var Checker = await _assessmentPortalServices.SaveFile(fileProperty, Username(), AssessmentSetUpId());
                    TempData["CreateQuest"] = "File Uploaded, It will be pending for approval";


                }
                catch (Exception ex)
                {
                    TempData["FailedQuest"] = "Error in adding question, Pls, Contact the admin";
                }
            else
            {

                TempData["CreateQuest"] = "File not specified and will not be sent for approval";
            }
            return View(model);
        }


        [LogActionFilter]
        public async Task<ActionResult> GetAllQuestionsAsync()
        {
            //string username = new CookieController().ReadFromCookie("Username");
            var assSchId = AssessmentSetUpId();
            if (assSchId==0)
                return RedirectToAction("Login", "Account");
            var result = await _assessmentPortalServices.GetAllRecordAllQuestions(Username(), assSchId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        [LogActionFilter]
        public async Task<ActionResult> SaveFinalAssessment()
        {
            //string username = new CookieController().ReadFromCookie("Username");
            var assSchId = AssessmentSetUpId();
            if (assSchId == 0)
                return RedirectToAction("Login", "Account");
            var result = await _assessmentPortalServices.SaveFinalAssessment(Username(), assSchId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadAnswerAsync(HttpPostedFileBase fileBase )
        {
            try
            {
                var records = String.Empty;

                if (Request.Files.Count > 0)
                {  //  Get all files from Request object  
                    string catId = Request["CategoryId"];
                    string questionId = Request["QuestionId"];
                    int categoryID = int.Parse(catId);
                    int questId= int.Parse(questionId);
                    HttpFileCollectionBase Files = Request.Files;
                    HttpPostedFileBase FileUpload = Files[0];
                   
                    
                    // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                    // if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    if (FileUpload != null && FileUpload.ContentLength > 0)
                    {
                        string filename = FileUpload.FileName;
                        string targetpath = Server.MapPath("~/Doc/");
                        if (!Directory.Exists(targetpath))
                        {
                            Directory.CreateDirectory(targetpath);
                        }
                        string absolutePath = "quiz" + DateTime.Now.ToString("MMddyyyyHHmmss") + filename;
                        string pathToExcelFile = targetpath + absolutePath;
                        string domainPath = ConfigurationManager.AppSettings["DomainName"];
                        FileUpload.SaveAs(pathToExcelFile);
                        var answerModel = new FileUploadVM
                        {
                            FilePath = "/Doc/" + absolutePath,
                            QuestionCategoryId = categoryID,
                            QuestionId= questId
                        };
                        var Checker = await _assessmentPortalServices.SaveFile(answerModel, Username(), AssessmentSetUpId());
                        records = Checker ? pathToExcelFile : "failed";

                    }

                }
                return Json(new
                {
                    data = records
                }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                _loggerInfo.Error(ex.ToString(), "Upload Excel:::");
                return Json(new
                {
                    data ="Error While uploading the excel document"
                }, JsonRequestBehavior.AllowGet);
            }
        }
       
        public async Task<ActionResult> SaveQuestionByCategoryAsync(SaveQuestionVM questionVM )
        {
            var scheduleId = AssessmentSetUpId();
         var check=   await _assessmentPortalServices.SaveAssessment(questionVM, Username(), scheduleId);
            //redirect if the answer is no
          
            return Json(new
            {
                data = check,
                scheduleId,
            }, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> SaveQuestionByCorrectivePlanTest(SaveQuestionVM questionVM)
        {
            var scheduleId = AssessmentSetUpId();
            var check = await _assessmentPortalServices.SaveCorrectivePlan(questionVM, Username(), scheduleId);
            //redirect if the answer is no

            return Json(new
            {
                data = check,
                scheduleId,
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public async Task<JsonResult> GetAllResult(int QuestionCatId)
        {
            var records = await _assessmentPortalServices.GetAllSavedAssessment( Username(), QuestionCatId, AssessmentSetUpId());

            return Json(new
            {
                data = records
            }, JsonRequestBehavior.AllowGet);


        }
        [HttpGet]
        public async Task<JsonResult> GetAllUsersResult(int QuestionCatId)
        {
            var records = await _assessmentPortalServices.GetAllSavedAssessment(Username(), QuestionCatId, AssessmentSetUpId());

            return Json(new
            {
                data = records
            }, JsonRequestBehavior.AllowGet);


        }
        private int AssessmentSetUpId()
        {
            var aCookie = System.Web.HttpContext.Current.Request.Cookies["AssSchId"];
           return aCookie.Value != null ? int.Parse(aCookie.Value) : 0;
        }
        private string Username()
        {
            var user = new CookieController().ReadFromCookie("UserRole");
            var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
            return deUser.Username;
        }


    }
}