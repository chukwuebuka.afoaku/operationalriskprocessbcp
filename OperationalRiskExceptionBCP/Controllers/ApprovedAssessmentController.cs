﻿using NLog;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class ApprovedAssessmentController : Controller
    {

        private IAssessmentPortalServices _assessmentPortalServices;
    

        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

        public ApprovedAssessmentController()
        {
           
            _assessmentPortalServices = new AssessmentPortalServices();
        }
        // GET: ApprovedAssessment
        [CustomAuthorize(UserRoleEnum.OperationalRisk)]
        public async Task<ActionResult> ApprovedTest(ApproveFlowVM model)
        {
            var result = await _assessmentPortalServices.Approved(model.UserId, model.SetupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize(UserRoleEnum.OperationalRisk)]
        public async Task<ActionResult> RejectedTest(ApproveFlowVM model)
        {
            var result = await _assessmentPortalServices.Rejected(model.UserId, model.SetupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}