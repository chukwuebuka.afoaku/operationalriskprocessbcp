﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    [CustomAuthorize(UserRoleEnum.OperationalRisk)]
    public class OperationalRiskController : Controller
    {
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        // GET: OperationalRisk
        private readonly ITestResultService _testresultservice;

        // Question

        private readonly IQuestionCategoryService _questionCategory;
        private readonly IQuestionService _question;


        private IAssessmentPortalServices _assessmentPortalServices;
        private IDashboardService _dashboardService;

       

        public OperationalRiskController()
        {
            _assessmentPortalServices = new AssessmentPortalServices();
            _dashboardService = new DashboardService();
            _testresultservice = new TestResultService();

            _questionCategory = new QuestionCategoryService();
            _question = new QuestionService();
        }
        [LogActionFilter]
        public ActionResult Index(string StartDate="", string EndDate="")
        {
            DateTime startDate=DateTime.Now;
            DateTime endDate = DateTime.Now;
            if (!String.IsNullOrEmpty(StartDate))
            {
                startDate = DateTime.Parse(StartDate);
            }
            else
            {
                ViewBag.StartDate =(new DateTime(DateTime.Now.Year,1,1)).ToString("dd-MMM-yyyy");
            }

            if (!String.IsNullOrEmpty(EndDate))
            {
                endDate = DateTime.Parse(EndDate);
            }
            else
            {
                ViewBag.EndDate =DateTime.Now.ToString("dd-MMM-yyyy");
            }
            // var totalScore= _assessmentPortalServices.GetAllScores();
            var model = _testresultservice.PendingAssessments(startDate, endDate);
            return View(model);
        }
        [LogActionFilter]
        public ActionResult UserScript(int userId, int SetupId)
        {
            var binder = new ScriptVM() {
                UserId=userId,
                SetupId=SetupId
            };

            return View(binder);
        }

        [LogActionFilter]
        public ActionResult GeneralScript(int userId, int SetupId)
        {
            var binder = new ScriptVM()
            {
                UserId = userId,
                SetupId = SetupId
            };

            return View(binder);
        }

        //public ActionResult UserResult(string Category=null)
        //{
        //    //var model = new AssessmentReviewVM()
        //    //{
        //    //    UserId = userId,
        //    //    AssessmentSetupId = assessmentSetupId
        //    //};
        //    //return View(model);
        //    List<Question> questions;
        //    List<QuestionCategory> questionCategories = _questionCategory.AllQuestionCategoriesPart().ToList();

        //    if (Category == null)
        //    {
        //        questions = _question.GetAllQuestion().ToList();
        //    }
        //    else
        //    {
        //        questions =  _questionCategory.AllQuestionCategoriesPart().Where(p => p.CategoryName == Category).ToList();


        //    }

        //    ResultListVM model = new ResultListVM();
        //    model.Questions = questions;
        //    model.QuestionCategories = questionCategories;

        //    return View(model);
        //}


        //Implementing shop method
        //public ActionResult UserResult2(string Category=null)
        //{
        //    List<QuestionVM> questions;
        //    List<QuestionCategoryVM> categories = _questionCategory.GetAllQuestionCategories().ToList();

        //    if (Category == null)
        //    {
        //        questions = _question.GetQuestion(int Id);
        //    }
        //    return View();
        //}


        public ActionResult GetDataDashboard()
        {
          //  var result = null;//_testresultservice.PendingAssessments();
            return Json(new
            {
                data = new { }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Dashboard()
        {
            var result = _dashboardService.DashBoard();
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetUserRecords(int userId, int setupId)
        {
            var result = _assessmentPortalServices.GetAllSavedAssessments(userId,  setupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult UserCorrectivePlan(int userId, int SetupId)
        {

            var allRecords = _assessmentPortalServices.getAllCorrectivePlan(userId, SetupId);
            var binder = new ScriptVM()
            {
                UserId = userId,
                SetupId = SetupId,
                Plans=allRecords,
            };

            return View(binder);
        }
        public ActionResult GetUserCorrectiveRecords(int userId, int setupId)
        {
            var result = _assessmentPortalServices.GetAllSavedAssessments(userId, setupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }

       
        public JsonResult Pending(DataTableAjaxPostModel model)
        {
            int filteredResultsCount;
            int totalResultsCount;

            var result =  _testresultservice.GetAllTestDataTablePending(model, out filteredResultsCount, out totalResultsCount, Username(), Core.Entities.ApprovalStatus.Pending);
            return Json(new
            {
                model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            }, JsonRequestBehavior.AllowGet);
        }


        private string Username()
        {
            var user = new CookieController().ReadFromCookie("UserRole");

            var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
            return deUser.Username;
        }



        public ActionResult Approve(int Id)
        {
            var model = new ApprovalVM
            {
                ApprovalStatus = Core.Entities.ApprovalStatus.Approved,
                QuestionId = Id,
                Username = Username()

            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Approve(ApprovalVM model)
        {
            try
            {
                await _testresultservice.TestApproval(model);
                return new JsonResult { Data = new { status = true } };
            }
            catch (Exception ex)
            {

            }
            return new JsonResult { Data = new { status = false } };


        }
        public ActionResult Reject(int Id)
        {
            var model = new ApprovalVM
            {
                ApprovalStatus = Core.Entities.ApprovalStatus.Approved,
                QuestionId = Id,
                Username = Username()

            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Reject(ApprovalVM model)
        {
            try
            {
                await _testresultservice.TestApproval(model);
                return new JsonResult { Data = new { status = true } };
            }
            catch (Exception ex)
            {

            }
            return new JsonResult { Data = new { status = false } };

        }


        public ActionResult UserGuide()
        {
            return View();
        }


        public ActionResult ApprovedChampionList()
        {
            var model = _testresultservice.ApprovedAssessments();
            return View(model);
        }


        //Chuks assignment
        public async Task<ActionResult> ApprovedChListAsync()
        {
            var model = _testresultservice.ApprovedAssessments();
            var excel = GenerateToExcel.GenerateExcel<AssessmentReviewVM>(model);

            Response.Clear();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "tpms_dict.xls"));
            Response.BinaryWrite(excel.GetBuffer());

            Response.End();

            return Content("Downloaded");



        }

public ActionResult RejectedChampionList()
        {
            var model = _testresultservice.RejectedAssessments();
            return View(model);
        }

        public async Task<ActionResult> RejectedChampionListExcel()
        {
            var model = _testresultservice.RejectedAssessments();
            var excel = GenerateToExcel.GenerateExcel<AssessmentReviewVM>(model);
            var fileName = "ApprovedExcelDoc.xlsx";
            using (var fileStream = new FileStream(Path.Combine(Server.MapPath("~/Content/Doc"), fileName), FileMode.Open))
            {
                await fileStream.CopyToAsync(excel);
            }
            excel.Position = 0;
            return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);





        }
    }
}