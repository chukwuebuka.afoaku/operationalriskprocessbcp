﻿using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    [CustomAuthorize(UserRoleEnum.OperationalRisk)]
    public class AssessmentSchedulingController : Controller
    {
        // GET: AssessmentScheduling
        private readonly IAssessmentSchedulerServices _assessmentScheduler;

        public AssessmentSchedulingController()
        {
            _assessmentScheduler = new AssessmentSchedulerServices();
        }
        [LogActionFilter]
        public ActionResult Home()
        {

            
            return View();
        }
        [HttpPost]
        [LogActionFilter]
        public async Task<ActionResult> Home(AssessmentSchedulerVM model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = "";
                var check = await _assessmentScheduler.CreateAddUser(model);
                if (check)
                {
                    TempData["CreateCat"] = "  Successfully Added user(s)";
                    return View();
                }
                else
                {
                    TempData["Failed"] = "Error while trying to add users, Please check if the period name is not recurring on the system, or contact the admin";
                    return View(model);
                }

            }

            return View();
        }
        [LogActionFilter]
        public ActionResult Update(int Id)
        {
            var item = _assessmentScheduler.GetAssessmentSetup(Id);

            var modelBinder = new AssessmentSchedulerVM
            {
                Id = item.Id,
                AssessmentSetupId = item.Id.ToString(),
                //The AssessmentSchedulerName is not the real name just to bind;
                AssessmentSchedulerName = item.AssessmentName
            };
            return View(modelBinder);
        }
        [HttpPost]
        [LogActionFilter]
        public async Task<ActionResult> Update(AssessmentSchedulerVM model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = "";
                var check = await _assessmentScheduler.CreateUpdate(model);
                if (check)
                {
                    TempData["CreateCat"] = "  Successfully Updated user(s)";
                    return RedirectToAction("Home");
                }
                else
                {
                    TempData["Failed"] = "Error while trying to add users, Please check if the period name is not recurring on the system, or contact the admin";
                    return RedirectToAction("Update", new { Id = model.Id });
                }

            }

            return RedirectToAction("Update", new { Id = model.Id });
        }
        public JsonResult GetAllPeriod(string searchTerm, int pageSize, int pageNumber)
        {
            var result = _assessmentScheduler.GetAllAssessmentPeriod(searchTerm, pageSize, pageNumber);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllUsers(string searchTerm, int pageSize, int pageNumber)
        {
            var result = _assessmentScheduler.GetAllUsers(searchTerm, pageSize, pageNumber);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllRecords(DataTableAjaxPostModel model)
        {
            var result = _assessmentScheduler.GetAllDataTable(model,
                out int filteredResultsCount, out int totalResultsCount);
            return Json(new
            {
                model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUserbyAssessment(int id)
        {
            var records = _assessmentScheduler.GetAllUserForAssessment(id);
            var reformattedRecords = records.Select(x => new {
                Text = x.Name,
                Id = x.Value.ToString()
            });
            return Json(new
            {
                data = reformattedRecords
            }, JsonRequestBehavior.AllowGet); ;
        }

    }
}