﻿using Newtonsoft.Json;
using OperationalRiskException.Controllers;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.AuthorizationFilter
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class CustomAuthorize : AuthorizeAttribute
    {
        private string[] UserCustomerRoles { get; set; }

        public CustomAuthorize(params object[] userProfilesRequired)
        {
            //if (userProfilesRequired == null) return;
            if (userProfilesRequired.Any(p => p.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("UserCustomerRoles");

            UserCustomerRoles = userProfilesRequired.Select(p => Enum.GetName(p.GetType(), p)).ToArray();
        }
        public override void OnAuthorization(AuthorizationContext context)
        {
            //string strPreviousPage = string.Empty;

            //if (HttpContext.Current.Request.UrlReferrer != null)
            //{
            //    strPreviousPage = HttpContext.Current.Request.UrlReferrer.Segments[HttpContext.Current.Request.UrlReferrer.Segments.Length - 1];
            //}

            //if (strPreviousPage == "")
            //{
            //    context.Result = new RedirectResult(Utility.GetBaseURL() + "Account/Login");
            //}

            if (UserCustomerRoles.Length == 0) return;
            var userRole = new CookieController().ReadFromCookie("UserRole"); 
            var roleView = string.IsNullOrWhiteSpace(userRole) ? new RoleViewModel() : JsonConvert.DeserializeObject<RoleViewModel>(userRole);
            var authorized = UserCustomerRoles.Any(role => role.Equals(roleView?.Rolename, StringComparison.CurrentCultureIgnoreCase));

            if (authorized)
            {
                //AddToCookie(roleView, "UserRole");
                return;
            }
            string url = HttpContext.Current.Request.Url.ToString();
            var redirectUrl = new UrlHelper(context.RequestContext);
            var logonUrl = string.IsNullOrWhiteSpace(userRole) ? redirectUrl.Action("Login", "Account") : redirectUrl.Action("index", "ErrorPag"); //, new { returnUrl = url, Area = "" });
            context.Result = new RedirectResult(logonUrl);
        }

        private static void AddToCookie(RoleViewModel roleView, string key)
        {
            var sessionTimeout = Convert.ToDouble(ConfigurationManager.AppSettings["sessionTimeout"]);
            var userCookies = new System.Web.HttpCookie(key)
            {
                Value = JsonConvert.SerializeObject(roleView),
                Expires = DateTime.Now.AddMinutes(sessionTimeout)
            };
            // userCookies.Secure = true;
            userCookies.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(userCookies);
        }

    }
}