﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.AuthVM
{
    public class AuthenticationResponce
    {
        public string isSuccessful { get; set; }
        public string response { get; set; }
        public string errorResponseMessage { get; set; }
        public string errorResponseCode { get; set; }

    }
}
