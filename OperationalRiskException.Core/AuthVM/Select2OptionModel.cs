﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.AuthVM
{
    public class Select2OptionModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public int AssId { get; set; }
    }
}
