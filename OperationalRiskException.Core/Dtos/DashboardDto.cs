﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Dtos
{
    public class DashboardDto
    {
        public int NoOfChampions { get; set; }
        public int NotYetTakenAssessments { get; set; }
        public int TakenAssessments { get; set; }
    }
}
