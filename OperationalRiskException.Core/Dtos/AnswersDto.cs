﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Dtos
{
   public class AnswersDto
    {
       
        public int QuestionId { get; set; }
        public string QuestionTest { get; set; }
        public string FilePath { get; set; }
        public string QuestionFilePath { get; set; }
        public string AnswerPath { get; set; }
        public string AttachmentName { get; set; }
    }

    public class MainAnswerDto
    {
       
        public string CategoryName { get; set; }
       public List< AnswersDto> answers { get; set; }
        public int CategoryId { get; set; }
    }
}
