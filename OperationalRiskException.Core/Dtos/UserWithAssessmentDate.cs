﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Dtos
{
   public class UserWithAssessmentDate
    {
        
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentId { get; set; }
        public string strUserRole { get; set; }
        public string ReliefEmail { get; set; }
        public string SupervName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       

    }
}
