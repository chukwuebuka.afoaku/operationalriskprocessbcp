﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class QuestionVM
    {
        public int QuestionId { get; set; }
		[Required(ErrorMessage = "Select Question Category")]
		public int CategoryId { get; set; }
		//[Required(ErrorMessage = "Enter Question in the box")]
		//[StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string QuestionText { get; set; }
		public string BatchNo { get; set; }
		public string CategoryName { get; set; }
        public string CreateUser { get; set; }

		[Required(ErrorMessage = "Enter Attachment name in the box")]
		[StringLength(1000, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string AttachmentName { get; set; }
		public string AttachmentUpload { get; set; }
		public string ApprovalUser { get; set; }
	}
}
