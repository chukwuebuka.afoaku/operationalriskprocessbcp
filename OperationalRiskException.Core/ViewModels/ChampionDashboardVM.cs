﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class ChampionDashboardVM
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public bool IsTestAvailable { get; set; }
        public int  NoOfTestTaken { get; set; }
        public List<TestResult> TestResults { get; set; }
        public List<AvailableAssessment> ListOfAvailableTest { get; set; }
        public List<ScoreVM> Scores{ get; set; }

    }
    public class TestResult
    {
       public string AssessmentName { get; set; }
        public int Score { get; set; }
        public int TotalQuestion { get; set; }

    }
    public class AvailableAssessment
    {
        public int AssessmentScheduleId { get; set; }
        public string AssessmentName { get; set; }
    }
}
