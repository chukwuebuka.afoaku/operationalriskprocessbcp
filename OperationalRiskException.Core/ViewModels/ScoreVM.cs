﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class ScoreVM
    {
        public string Username { get; set; }
        public int ScoreYes { get; set; }
        public int TotalScore { get; set; }
        public string Department { get; set; }
  
        public int ScoreNo { get; set; }
        public double Risk { get; set; }
        public int AssessmentSetupId { get; set; }
        public int UserId { get; set; }

    }
}
