﻿using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class AssessmentSetupVM : BaseVM
    {
        public int Id { get; set; }
        [Required]
        public string AssessmentName { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [Required]
   
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        [Required(ErrorMessage = "You must select at least one question categories")]
        [RequiredArray(ErrorMessage = "You must select at least one question categories")]
        public string[] ListOfQuestionCategories { get; set; }
        public string StartDateString => StartDate.ToLongDateString();
        public string EndDateString => EndDate.ToLongDateString();
        public IEnumerable<AssessmentUserSetup> Users { get; set; }
    }
}
