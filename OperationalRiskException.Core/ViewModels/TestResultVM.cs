﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class TestResultVM
    {
        public string Username { get; set; }
        public string Department { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentUpload { get; set; }
        public int AssessmentSetupId { get; set; }
        public int UserId { get; set; }

    }
}
