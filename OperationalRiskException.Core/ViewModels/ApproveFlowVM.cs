﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class ApproveFlowVM
    {
        public int UserId { get; set; }
        public int SetupId { get; set; }
    }
}
