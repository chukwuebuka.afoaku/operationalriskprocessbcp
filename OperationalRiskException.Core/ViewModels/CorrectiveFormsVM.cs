﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class CorrectiveFormsVM
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
    }

}
