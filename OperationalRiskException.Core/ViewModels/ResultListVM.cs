﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperationalRiskException.Core.Entities;

namespace OperationalRiskException.Core.ViewModels
{
    public class ResultListVM
    {
        public IEnumerable<Question> Questions { get; set; }
        public IEnumerable<QuestionCategory> QuestionCategories { get; set; }
    }
}
