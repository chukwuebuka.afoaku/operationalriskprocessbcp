﻿using OperationalRiskException.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class AssessmentReviewVM:BaseVM
    {
        public int Id { get; set; }
        public string AssessmentSchedulerName { get; set; }
        public int AssessmentSetupId { get; set; }
       
        public string Username { get; set; }
        //public string SOLID { get; set; }
        public int UserId { get; set; }
        public bool IsAnswered { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public string ApprovalStatusString => ApprovalStatus.ToString();
        public string ApprovedBy { get; set; }
        public string RejectedBy { get; set; }
        public string Department { get; set; }

        // Date of conclusion
        public DateTime? AssessmentDateCompleted { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? RejectedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public string StartDateString => StartDate.ToLongDateString();
        public string EndDateString => EndDate.ToLongDateString();



    }
    //public class ReportViewDashboard
    //{

    //    public string StartDate { get; set; }
    //    public  string EndDate { get; set; }
    //    public IEnumerable<AssessmentReviewVM> AssessmentReviewVMs { get; set; }
    //}

    public class AssessmentReport
    {
   

        public string Username { get; set; }


     
    
        public string ApprovedBy { get; set; }
 
        public string Department { get; set; }
        public string SOLID { get; set; }

        public DateTime? DateCompleted { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string RejectedBy { get; set; }
        public DateTime? RejectedDate { get; set; }
        public string ApprovalStatus { get; set; }

       
    }
}
