﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class AddUserVM :BaseVM
    {
		[Required(ErrorMessage = "Enter Username")]

		public string Username  { get; set; }

	
		public UserRoleEnum UserRole  { get; set; }
        public IEnumerable<UserRoleEnum> ListUserRoles { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentId { get; set; }
        public bool? IsDeleted { get; set; }
        public int Id { get; set; }
        public string SolID { get; set; }
    }
}
