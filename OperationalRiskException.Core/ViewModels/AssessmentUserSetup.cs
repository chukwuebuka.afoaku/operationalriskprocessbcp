﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class AssessmentUserSetup
    {
        public string Username { get; set; }
        public int UserId { get; set; }
        public string Department { get; set; }
    }
}
