﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class DivisionVM
    {
        public string solID { get; set; }
        public string CountryName { get; set; }
        public string departmentName { get; set; }
        public string Department { get; set; }
        public string OfficeAddress { get; set; }
        public string BankBranchID { get; set; }
        public string BankBranchName { get; set; }
        public string directorateName { get; set; }
        public string Country2 { get; set; }
        public string divisionName { get; set; }
    }
}
