﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class TestQuestionVM
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentUpload { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
