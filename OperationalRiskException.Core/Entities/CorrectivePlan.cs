﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class CorrectivePlan: BaseEntity
    {
        public int Id { get; set; }
        public string CorrectiveText { get; set; }
        public int QuestionId{ get; set; }
        public Question Question { get; set; }
        public int CorrectivePlanSetupId { get; set; }
        public CorrectivePlanSetup CorrectivePlanSetup { get; set; }

    }
}
