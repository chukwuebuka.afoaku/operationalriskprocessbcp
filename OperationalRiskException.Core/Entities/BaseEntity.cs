﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OperationalRiskException.Core.Entities
{
    public abstract class BaseEntity
    {
    
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
