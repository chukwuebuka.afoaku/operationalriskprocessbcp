﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace OperationalRiskException.Core.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
         //   this.AssessmentSetups = new HashSet<AssessmentSetup>();
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string  FirstName { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentId { get; set; }
        public UserRoleEnum UserRole { get; set; }
        public string strUserRole { get; set; }
        public string  ReliefEmail { get; set; }
        public string SupervName { get; set; }
        public string SOLId { get; set; }

        /// public ICollection<AssessmentSetup> AssessmentSetups { get; set; }

    }
}
