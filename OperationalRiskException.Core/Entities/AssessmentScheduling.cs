﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class AssessmentScheduling : BaseEntity
    {
        public int Id { get; set; }
        public string AssessmentSchedulerName { get; set; }
        public int AssessmentSetupId { get; set; }
        public AssessmentSetup AssessmentSetup { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public bool IsAnswered { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public string ApprovedBy { get; set; }
        public string RejectedBy { get; set; }
        public DateTime? ApprovedDate{get; set;}

        public DateTime? RejectedDate { get; set; }

        public DateTime? AssessmentDateCompleted { get; set; }


    }
}
