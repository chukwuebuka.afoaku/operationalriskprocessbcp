﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class LoginLockout:BaseEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime AttemptedDateTime { get; set; }
        public int AccessFailedCount { get; set; }
    }
}
