﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
   public class AssessmentSetup : BaseEntity
    {
        public AssessmentSetup()
        {
      
        }
          
        public int Id { get; set; }
        public string AssessmentName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       
        public string QuestionCategories { get; set; }
      //  public  ICollection<User> Users { get; set; }


    }
}
