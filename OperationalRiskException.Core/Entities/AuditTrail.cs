﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
	public class AuditTrail:BaseEntity
	{
        public AuditTrail() { }
        public long Id { get; set; }
        public string PerformedOn { get; set; }
        public string PerformedBy { get; set; }
        public string Role { get; set; }
        public string Ipaddress { get; set; }
        public string ComputerName { get; set; }
        public string ActionType { get; set; }
        public string Comments { get; set; }
        public string moduleName { get; set; }
    }
}
