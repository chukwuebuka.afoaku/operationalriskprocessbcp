﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
	public class BatchUpload : BaseEntity
	{
		public int Id { get; set; }
		public QuestionCategory Category{ get; set; }
		public int CategoryId { get; set; }
		public string BatchNo { get; set; }
		public string FileName { get; set; }
        public int NumberOfRecords { get; set; }
	}
}
