﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
  public  class AnswerEntity :BaseEntity
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string FilePath { get; set; }
        public string AttachmentName { get; set; }
        public AssessmentSetup AssessmentSetup { get; set; }
        public int AssessmentSetupId { get; set; }
        public User User{ get; set; }
        public int UserId { get; set; }
        public int QuestionCategoryId { get; set; }
        public QuestionCategory QuestionCategory { get; set; }
        public ApprovalStatusTest ApprovalStatusTest { get; set; }
    }



    public class ApprovalTest : BaseEntity
    {
        public long Id { get; set; }
        public Question Question { get; set; }
        public int QuestionId { get; set; }
        //public string QuestionHistory { get; set; }
        //public int? QuestionCreatedById { get; set; }
        //public User QuestionCreatedBy { get; set; }
        public int? ApprovedOrRejectById { get; set; }
        public User ApprovedOrRejectBy { get; set; }
        public ApprovalStatusTest ApprovalStatus { get; set; }
        public bool IsActive { get; set; }
    }
    public enum ApprovalStatusTest
    {
        Pending = 0,
        Approved,
        Rejected

    }



}
