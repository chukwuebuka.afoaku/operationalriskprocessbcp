﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class CompletedAssessment : BaseEntity
    {
        public int Id { get; set; }
        public DateTime? AssessmentDateCompleted { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public int AssessmentSetupId { get; set; }
        public AssessmentSetup AssessmentSetup { get; set; }
        //public int AnswerEntityId { get; set; }
        //public AnswerEntity AnswerEntity { get; set; }
    }
}
    