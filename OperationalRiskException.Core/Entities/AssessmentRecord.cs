﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
  public  class AssessmentRecord :BaseEntity
    {
       public int Id { get; set; }
      
        public QuestionCategory QuestionCategory { get; set; }
        public int QuestionCategoryId { get; set;}
        public int UserId { get; set; }
        public int AssessmentSetupId { get; set; }
        public AssessmentSetup AssessmentSetup { get; set; }
        public bool IsAnswered { get; set; }


    }
}
