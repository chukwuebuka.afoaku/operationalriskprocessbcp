﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class QuestionCategory : BaseEntity
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        
    }
}
