﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Enums
{
    public enum UBACountries
    {
        [Description("Nigeria")]
        NGA = 0,
        [Description("BENIN")]
        BEN,
        [Description("BURKINA FASO")]
        BFA,
        [Description("CAMEROON")]
        CMR,
        [Description("CHAD")]
        TCD,
        [Description("CONGO BRAZZAVILLE")]
        COG,
        [Description("CONGO DRC")]
        COD,
        [Description("COTE D'IVOIRE")]
        CIV,
        [Description("GABON")]
        GAB,
        [Description("GHANA")]
        GHA,
        [Description("GUINEA")]
        GIN,
        [Description("KENYA")]
        KEN,
        [Description("LIBERIA")]
        LBR,
        [Description("MALI")]
        MLI,
        [Description("MOZAMBIQUE")]
        MOZ,

        [Description("SENEGAL")]
        SEN,
        [Description("SIERRA LEONE")]
        SLE,
        [Description("TANZANIA")]
        TZA,
        [Description("UGANDA")]
        UGA,
        [Description("ZAMBIA")]
        ZMB
    }

}