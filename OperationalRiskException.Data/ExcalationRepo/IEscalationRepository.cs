﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.EscalationRepo
{
    public interface IEscalationRepository
    {
        Core.AuthVM.HRStaffObject GetSTAFF_USER(string username);
        int InsertFirstEscalationRecords(List<Core.Dtos.ChampionEscalationDTO> championEscalationLists);
        List<Core.Dtos.ONEscalationChampionDTO> OnEscalationList();
        List<Core.Dtos.ChampionEscalationDTO> PendingEscalation();
        bool SaveFirstEscalation(string supervUser, string SupervisorRole, Core.Dtos.ChampionEscalationDTO escalationDTO);
        bool SaveOtherEscalation(string supervUser, string SupervisorRole, Core.Dtos.ONEscalationChampionDTO pendingItem);
        List<Core.Dtos.ChampionEscalationDTO> WarningEscalation();
    }
}
