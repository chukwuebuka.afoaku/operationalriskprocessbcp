﻿using OperationalRiskException.Data.GenericRepository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<T> Repository<T>() where T : class;
        void RollBack();
        void BeginTransaction();
        void Commit();
        Task<int> SaveChangesAsync();
        
    }
}
