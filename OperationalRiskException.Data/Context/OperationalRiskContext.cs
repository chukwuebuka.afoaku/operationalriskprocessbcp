﻿using EntityFramework.Filters;
using OperationalRiskException.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.Context
{
    public class OperationalRiskContext : DbContext
    {
     

        public OperationalRiskContext() :base("OperationalRiskBCPContext") { }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    DbInterception.Add(new FilterInterceptor());
        //    var softDeleteFilter = FilterConvention.Create<BaseEntity>("SoftDelete",
        //        e => e.IsDeleted == false); // don't change it into e => !e.IsDeleted
        //    modelBuilder.Conventions.Add(softDeleteFilter);
        //   // base.OnModelCreating(modelBuilder);
        //}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
           /* modelBuilder.Entity<User>()
               .HasMany<AssessmentSetup>(s => s.AssessmentSetups)
               .WithMany(c => c.Users)
               .Map(cs =>
               {
                   cs.MapLeftKey("UserRefId");
                   cs.MapRightKey("AssessmentSetupRefId");
                   cs.ToTable("UserAssessmentSetupCourse");
               });*/
        }

        public DbSet<User> Users { get; set; }
        public DbSet<QuestionCategory> QuestionCategories { get; set; }
        public DbSet<Question> Questions { get; set; }
		public DbSet<BatchUpload> BatchUploads { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<AssessmentSetup> AssessmentSetups { get; set; }
        public DbSet<AssessmentScheduling> AssessmentSchedulings { get; set; }
        public DbSet<AssessmentRecord> AssessmentRecords { get; set; }
        public DbSet<AnswerEntity> AnswerEntities { get; set; }
      //  public DbSet<CorrectivePlan> CorrectivePlans { get; set; }
      //  public DbSet<CorrectivePlanSetup> CorrectivePlanSetups { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<Approval> Approvals { get; set; }
        public DbSet<SaveAssessmentByCategory> SaveAssessmentByCategories { get; set; }
        public DbSet<Escalation> Escalations { get; set; }
        public DbSet<EscalationProgress> EscalationProgresses { get; set; }
        public DbSet<LoginLockout> loginLockouts { get; set; }
        //public DbSet<CompletedAssessment> CompletedAssessments { get; set; }
    }
}
