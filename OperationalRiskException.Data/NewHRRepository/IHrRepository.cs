﻿using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.ViewModels;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.NewHRRepository
{
    public interface IHrRepository
    {
        Task<HRStaffObject> GetUserProfile(string username);
    }
}
