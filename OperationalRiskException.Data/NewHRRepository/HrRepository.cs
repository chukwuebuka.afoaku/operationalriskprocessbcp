﻿using Dapper;
using NLog;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.NewHRRepository
{
    public class HrRepository: IHrRepository
    {
        readonly IDbConnection _sqldb;
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public HrRepository()
        {

            _sqldb = ConnectionFactory.GetHRSqlConnectionApproval();

        }
        public async Task<HRStaffObject> GetUserProfile(string username)
        {
            var sqlQuery = $"SELECT * FROM vw_HR_STAFF_SUPERVISOR_NEW WHERE username = @username";

            var result =await _sqldb.QueryFirstOrDefaultAsync<HRStaffObject>(sqlQuery, new { username = username }, commandType: CommandType.Text);
            return result;
        }

    }
}
