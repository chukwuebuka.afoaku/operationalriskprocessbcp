﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.GenericRepository.IRepository
{
    public interface IRepository<TEntity> where TEntity : class
    {
       Task< IEnumerable<TEntity>> GetAllAsync();
       Task<TEntity> GetByIdAsync(object Id);
        void Insert(TEntity obj);
		void InsertList(List<TEntity> obj);

		void Update(TEntity obj);
        void Delete(TEntity obj);
        IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters);
       IEnumerable<TEntity> GetAllRecordsFilters(Expression<Func<TEntity, bool>> filter);
        TEntity GetSingleRecordFilter(Expression<Func<TEntity, bool>> filter);
       Task< TEntity> GetSingleRecordFilterAsync(Expression<Func<TEntity, bool>> filter);
        Task<int> SaveAsync();
		int Save();
		//chuks
		IEnumerable<TEntity> GetAll(object p);

        IQueryable<TEntity> GetAllIncluding(Expression<Func<TEntity, bool>> predicate, bool track, params Expression<Func<TEntity, object>>[] properties);

    }
}
