﻿using EntityFramework.Filters;
using OperationalRiskException.Data.Context;
using OperationalRiskException.Data.GenericRepository.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using OperationalRiskException.Core.Helpers;
using Newtonsoft.Json;
using OperationalRiskException.Core.ViewModels;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace OperationalRiskException.Data.GenericRepository.Repository
{
  
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private OperationalRiskContext dbContext;
        private DbSet<TEntity> dbSet;
	//	private DbSet<List<TEntity>> entities;
        public Repository()
        {
            dbContext = new OperationalRiskContext();
            dbSet = dbContext.Set<TEntity>();
			//entities = dbContext.Set<List<TEntity>>();


		}
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            

            return await dbSet.ToListAsync();
        }
        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = dbSet;
            foreach (var includeProperty in includeProperties)
            {
                entities = entities.IncludeOptimized(includeProperty);
            }
            return entities;
        }

        public IQueryable<TEntity> GetAllIncluding(Expression<Func<TEntity, bool>> predicate, bool track, params Expression<Func<TEntity, object>>[] properties)
        {

            var entities = IncludeProperties(properties);

            return
                track ? entities.Where(predicate) :
                    entities.Where(predicate).AsNoTracking();
        }



        public async Task<TEntity> GetByIdAsync(object Id)
        {
            return await dbSet.FindAsync(Id);
        }
        public IEnumerable<TEntity> GetAllRecordsFilters(Expression<Func<TEntity, bool>> filter)
        {
          
            var records =  dbSet.Where(filter);
            return records;
        }
        public TEntity GetSingleRecordFilter(Expression<Func<TEntity, bool>> filter)
        {
          
            var records = dbSet.Where(filter).FirstOrDefault();
            return records;
        }
        public void Insert(TEntity obj)
        {

           
                obj.GetType().GetRuntimeProperty("CreatedBy").SetValue(obj, GetUsername());
                obj.GetType().GetRuntimeProperty("IsDeleted").SetValue(obj, false);
            obj.GetType().GetRuntimeProperty("CreatedDate").SetValue(obj, Helpers.CurrentDateTime);
            dbSet.Add(obj);
        }
		public void InsertList(List<TEntity> obj)
		{

			//entities.AddRange(obj);
		}
		public void Update(TEntity obj)
        {
            obj.GetType().GetRuntimeProperty("ModifiedBy").SetValue(obj, GetUsername());
            obj.GetType().GetRuntimeProperty("IsDeleted").SetValue(obj, false);
            obj.GetType().GetRuntimeProperty("ModifiedDate").SetValue(obj, Helpers.CurrentDateTime);
            dbContext.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(TEntity obj)
        {
            obj.GetType().GetRuntimeProperty("ModifiedBy").SetValue(obj, GetUsername());
            obj.GetType().GetRuntimeProperty("IsDeleted").SetValue(obj, true);
            obj.GetType().GetRuntimeProperty("ModifiedDate").SetValue(obj, Helpers.CurrentDateTime);
            dbContext.Entry(obj).State = EntityState.Modified;
        }
        public async Task<int> SaveAsync()
        {
        return  await  dbContext.SaveChangesAsync();
        }
		public int Save()
		{
			return  dbContext.SaveChanges();
		}
        private  string GetUsername(string key = "UserRole")
        {
            try
            {
                var aCookie = System.Web.HttpContext.Current.Request.Cookies[key];
                var userRole = aCookie != null ? aCookie.Value : string.Empty;
                if (!string.IsNullOrWhiteSpace(userRole))
                {
                    var user = JsonConvert.DeserializeObject<RoleViewModel>(userRole);
                    return user.Username;
                }
                return "Anonymous";
            }
            catch (Exception exx)
            {
                return "Anonymous";
            }

            }
        //public List<TEntity> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        //{

        //  //  var whereClause = BuildDynamicWhereClause(Db, searchBy);

        //    if (String.IsNullOrEmpty(searchBy))
        //    {

        //        sortBy = "Id";
        //        sortDir = true;
        //    }

        //    var result = dbSet
        //                   .OrderBy(sortBy, sortDir) // have to give a default order when skipping .. so use the PK
        //                   .Skip(skip)
        //                   .Take(take)
        //                   .ToList();


        //    filteredResultsCount = Db.DatabaseTableEntity.AsExpandable().Where(whereClause).Count();
        //    totalResultsCount = Db.DatabaseTableEntity.Count();

        //    return result;
        //}


        public virtual IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return dbSet.SqlQuery(query, parameters).ToList();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.dbContext != null)
                {
                    this.dbContext.Dispose();
                    this.dbContext = null;
                }
            }
        }


        //Chuks additionS
        public IEnumerable<TEntity> GetAll(object p)
        {
            return dbSet.ToList();
        }

        public async Task<TEntity> GetSingleRecordFilterAsync(Expression<Func<TEntity, bool>> filter)
        {
            var records =await dbSet.Where(filter).FirstOrDefaultAsync();
            return records;
        }
    }
}
