﻿namespace OperationalRiskException.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datecompletd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        FilePath = c.String(),
                        AttachmentName = c.String(),
                        AssessmentSetupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        QuestionCategoryId = c.Int(nullable: false),
                        ApprovalStatusTest = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionCategories", t => t.QuestionCategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.AssessmentSetupId)
                .Index(t => t.UserId)
                .Index(t => t.QuestionCategoryId);
            
            CreateTable(
                "dbo.AssessmentSetups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssessmentName = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        QuestionCategories = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionText = c.String(),
                        BatchNo = c.String(),
                        AttachmentName = c.String(),
                        AttachmentUpload = c.String(),
                        QuestionCategoryId = c.Int(),
                        ApprovalStatus = c.Int(nullable: false),
                        QuestionCreatedOrModifiedById = c.Int(nullable: false),
                        ApprovedById = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ApprovedById)
                .ForeignKey("dbo.QuestionCategories", t => t.QuestionCategoryId)
                .ForeignKey("dbo.Users", t => t.QuestionCreatedOrModifiedById, cascadeDelete: true)
                .Index(t => t.QuestionCategoryId)
                .Index(t => t.QuestionCreatedOrModifiedById)
                .Index(t => t.ApprovedById);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        FirstName = c.String(),
                        DepartmentName = c.String(),
                        DepartmentId = c.Int(),
                        UserRole = c.Int(nullable: false),
                        strUserRole = c.String(),
                        ReliefEmail = c.String(),
                        SupervName = c.String(),
                        SOLId = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Approvals",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        QuestionHistory = c.String(),
                        QuestionCreatedById = c.Int(),
                        ApprovedOrRejectById = c.Int(),
                        ApprovalStatus = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ApprovedOrRejectById)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.QuestionCreatedById)
                .Index(t => t.QuestionId)
                .Index(t => t.QuestionCreatedById)
                .Index(t => t.ApprovedOrRejectById);
            
            CreateTable(
                "dbo.AssessmentRecords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionCategoryId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        AssessmentSetupId = c.Int(nullable: false),
                        IsAnswered = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionCategories", t => t.QuestionCategoryId, cascadeDelete: true)
                .Index(t => t.QuestionCategoryId)
                .Index(t => t.AssessmentSetupId);
            
            CreateTable(
                "dbo.AssessmentSchedulings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssessmentSchedulerName = c.String(),
                        AssessmentSetupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsAnswered = c.Boolean(nullable: false),
                        ApprovalStatus = c.Int(nullable: false),
                        ApprovedBy = c.String(),
                        RejectedBy = c.String(),
                        ApprovedDate = c.DateTime(),
                        RejectedDate = c.DateTime(),
                        AssessmentDateCompleted = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSetups", t => t.AssessmentSetupId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.AssessmentSetupId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AuditTrails",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PerformedOn = c.String(),
                        PerformedBy = c.String(),
                        Role = c.String(),
                        Ipaddress = c.String(),
                        ComputerName = c.String(),
                        ActionType = c.String(),
                        Comments = c.String(),
                        moduleName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BatchUploads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        BatchNo = c.String(),
                        FileName = c.String(),
                        NumberOfRecords = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EscalationProgresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupervisorEmail = c.String(),
                        SupervisorRole = c.String(),
                        LevelOfEscalation = c.Int(nullable: false),
                        EscalationId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Escalations", t => t.EscalationId, cascadeDelete: true)
                .Index(t => t.EscalationId);
            
            CreateTable(
                "dbo.Escalations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LevelOfEscalation = c.Int(nullable: false),
                        ChampionEmail = c.String(),
                        HasEscalationStopped = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        AssessmentSchedulerId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssessmentSchedulings", t => t.AssessmentSchedulerId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AssessmentSchedulerId);
            
            CreateTable(
                "dbo.LoginLockouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        AttemptedDateTime = c.DateTime(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SaveAssessmentByCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        CategoryScore = c.Int(nullable: false),
                        TotalQuestion = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SaveAssessmentByCategories", "UserId", "dbo.Users");
            DropForeignKey("dbo.SaveAssessmentByCategories", "CategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.EscalationProgresses", "EscalationId", "dbo.Escalations");
            DropForeignKey("dbo.Escalations", "UserId", "dbo.Users");
            DropForeignKey("dbo.Escalations", "AssessmentSchedulerId", "dbo.AssessmentSchedulings");
            DropForeignKey("dbo.BatchUploads", "CategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AssessmentSchedulings", "UserId", "dbo.Users");
            DropForeignKey("dbo.AssessmentSchedulings", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropForeignKey("dbo.AssessmentRecords", "QuestionCategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AssessmentRecords", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropForeignKey("dbo.Approvals", "QuestionCreatedById", "dbo.Users");
            DropForeignKey("dbo.Approvals", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Approvals", "ApprovedOrRejectById", "dbo.Users");
            DropForeignKey("dbo.AnswerEntities", "UserId", "dbo.Users");
            DropForeignKey("dbo.AnswerEntities", "QuestionCategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.AnswerEntities", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "QuestionCreatedOrModifiedById", "dbo.Users");
            DropForeignKey("dbo.Questions", "QuestionCategoryId", "dbo.QuestionCategories");
            DropForeignKey("dbo.Questions", "ApprovedById", "dbo.Users");
            DropForeignKey("dbo.AnswerEntities", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropIndex("dbo.SaveAssessmentByCategories", new[] { "UserId" });
            DropIndex("dbo.SaveAssessmentByCategories", new[] { "CategoryId" });
            DropIndex("dbo.Escalations", new[] { "AssessmentSchedulerId" });
            DropIndex("dbo.Escalations", new[] { "UserId" });
            DropIndex("dbo.EscalationProgresses", new[] { "EscalationId" });
            DropIndex("dbo.BatchUploads", new[] { "CategoryId" });
            DropIndex("dbo.AssessmentSchedulings", new[] { "UserId" });
            DropIndex("dbo.AssessmentSchedulings", new[] { "AssessmentSetupId" });
            DropIndex("dbo.AssessmentRecords", new[] { "AssessmentSetupId" });
            DropIndex("dbo.AssessmentRecords", new[] { "QuestionCategoryId" });
            DropIndex("dbo.Approvals", new[] { "ApprovedOrRejectById" });
            DropIndex("dbo.Approvals", new[] { "QuestionCreatedById" });
            DropIndex("dbo.Approvals", new[] { "QuestionId" });
            DropIndex("dbo.Questions", new[] { "ApprovedById" });
            DropIndex("dbo.Questions", new[] { "QuestionCreatedOrModifiedById" });
            DropIndex("dbo.Questions", new[] { "QuestionCategoryId" });
            DropIndex("dbo.AnswerEntities", new[] { "QuestionCategoryId" });
            DropIndex("dbo.AnswerEntities", new[] { "UserId" });
            DropIndex("dbo.AnswerEntities", new[] { "AssessmentSetupId" });
            DropIndex("dbo.AnswerEntities", new[] { "QuestionId" });
            DropTable("dbo.SaveAssessmentByCategories");
            DropTable("dbo.LoginLockouts");
            DropTable("dbo.Escalations");
            DropTable("dbo.EscalationProgresses");
            DropTable("dbo.Departments");
            DropTable("dbo.BatchUploads");
            DropTable("dbo.AuditTrails");
            DropTable("dbo.AssessmentSchedulings");
            DropTable("dbo.AssessmentRecords");
            DropTable("dbo.Approvals");
            DropTable("dbo.QuestionCategories");
            DropTable("dbo.Users");
            DropTable("dbo.Questions");
            DropTable("dbo.AssessmentSetups");
            DropTable("dbo.AnswerEntities");
        }
    }
}
